# ZSO 2017/18
## [Zadanie 2 - HardDoom™ driver](http://students.mimuw.edu.pl/ZSO/PUBLIC-SO/2017-2018/_build/html/z2_driver/index-en.html)

Autor: Jacek Łysiak

## Opis rozwiązania (w skrócie)

Sterownik urządzenia HardDoom™ posiada następujące cechy:
- synchronizcja z użyciem `FENCE_WAIT`
- aktualizacja stanu kolejki za pomocą `PING_ASYNC`

Główna struktura urządzenia `struct hd_device` tworzona 
jest w `hd_probe`. Następuje inicjalizacja, urządzenie startuje i resetowany jest
kontekst urządzenia.

Funkcje zmieniające parametry rysowania oraz tablice stron `hd_ctx_*` są idempotentne.
Wołane są w metodach wykonujących poszczególne polecenia zawsze, ale zmieniają
stan rejestrów urządzenia tylko wtedy, gdy nowa wartość różni się od starej.

Każda instancja posiada dwa obiekty `dma pool`. Jeden alokuje pamięć dla DMA
w kawałkach o rozmiarze `0x1000` na wszystko poza mapami kolorów, dla których
jest oddzielne `dma pool` alokujące pamięć po `0x100`.

#### Kolejka poleceń 

Struktury: `structs/hd_queue.h`  
Implementacja: `hd_queue.*`  

`struct hd_queue` przechowuje stan kolejki oraz kolejki oczekiwania na
FENCE oraz wolne miejsce. Na początku alokowany jest bufor na 1024 polecenia 
i ustawiane na stałe polecenie `JUMP` na postatniej pozycji, skaczące na 
początek kolejki.

Dostępu do kolejki bronimy mutexem, a zmienne wewnątrz, narażone na IRQ,
są chronione spinlockiem z IRQ save/restore.

Moduł zaiwera metody do synchronizacji FENCE i dodawania poleceń (z oczekiwaniem)
w dwóch wersjach: interruptible i uninterruptible.   
Nieprzerywalne oczekiwanie na synchronizację
jest stosowane tylko w momencie, gdy chcemy zwolnić zasoby albo oczekujemy na
synchronizację z urządzeniem przed przejściem w stan uśpienia.

##### Oczekiwanie na wolne miejsce 

Trzymane są zmienne `b`, `l` i `async_counter`. Początkowo ustawione odpowiednio 
na 0, 0 i `ASYNC_IVAL`. `b` i `l` działają jak zwykle w buforze cyklicznym.
Natomiast każde włożenie polecenia do kolejki powoduje zmniejsze `async_counter`
o jeden, gdy spadnie do zera wstawiane jest polecenie `PING_ASYNC` i ponownie
ustawiamy licznik na `ASYNC_IVAL`, które jest równe 1/8 maksymalnej długości 
całej kolejki poleceń (makra konfigurujące różne rzeczy są w `hd_conf.h`).

Każde przerwanie pochodzące od `PONG_ASYNC` powoduje przesunięcie `b` 
i zmniejszenie `l`. Na koniec wybudzamy proces, który mógł zasnąć w oczekiwaniu
na miejsce. Proces oczekujący na miejsce w kolejce stale trzyma mutex, 
aby nie doszło do przeplotów poleceń. 

Metody korzystające z `hd_queue_push_cmd*` muszą odpowiednio zadbać o zajęcie
kolejki.

##### Synchronizacja z użyciem FENCE

W module `hd_queue` są dwie metody, które służą do synchronizacji
z urządzeniem: `hd_queue_sync` i `hd_queue_sync_interruptible`.  
Na początku każdej z metod pozyskujemy mutex kolejki. Generujemy następny 
numerek dla FENCE i trzymamy w `fence_sent`. Następnie robimy push polecenia
`FENCE(fence_sent)`. Należy zauważyć, że nigdy nie wrzucimy `FENCE` pomiędzy
polecenia rysujące w jakimś bloku (bo mutex). 

Przed zwolnieniem kolejki sprawdzamy wartości dwóch zmiennych: `fence_last` oraz
`fence_pending`. Jeśli w tym momencie są równe, oznacza to, że nie oczekujemy na
żadne zdarzenie synchronizacyjne i musimy rozpocząć nowe.
Ustawiamy `fence_pending` na `fence_sent`, ustawiamy `FENCE_WAIT`, sprawdzamy czy
już gotowe czy nie i albo zwalniamy kolejke i wychodziimy aktualizując `fence_last`
albo zwalniamy kolejkę i idziemy spać w oczekiwaniu, aż nasz `fence_last` zrówna
się z numerkiem, na który oczekujemy.

W przerwaniu aktualizujemy `fence_last` i ustawiamy nowe `FENCE_WAIT` jeśli
`fence_pending = fence_sent`.

Na koniec robimy `wake_up_all`, aby wybudzić wszystkich, którzy czekali na sync.

Wszystko jest odpowiednio synchronizowane spinlockami IRQ save/restore.  
Oczekiwania procesów na zajście warunków - standardowy pattern z `while`.

#### Walidacja

Wszelkie sprawy związane z walidacją są w `hd_validate.*`.  
Po prostu zbiór funkcji z różnymi warunkami.

#### Operacje na buforach

Struktury: `structs/hd_resources.h`
Implementacja: `hd_surf.c`, `hd_res_ops.c`

Każdy bufor posiada mutex, który nalezy posiąść, aby wykonać odczyt/zapis.
Ponadto każdy `SURFACE` wyposażony jest we flgę `dirty`.
Polecenia rysujące, jeśli cokolwiek udało się narysowac, zapalają `dirty`.
Żądanie odczytu najpierw zajmuje mutex bufora, aby nikt nie mógł do niego pisać.
Następnie, jeśli `dirty == 1` wykonuje synchronizację `FENCE` i ustawia 
`dirty` na 0. Następuje odczyt i zwalniamy mutex.
Sprawdzanie `dirty` zostało dodane ponieważ, np. Doom odczytuje bufory w partiach,
co oznaczałoby wysyłanie niepotrzebnych `FENCE`.

#### INTERLOCK

Każdy bufor ramki na dodatkową flagę `interlock`. Ponadto wszystkie bufory
są połączone w listę dwukierunkową.
Każda operacja, która korzysta z bufora jako `DST` czyści flagę `interlock`.
Operacja `COPY_RECT` zaraz po zajęciu mutexa bufora i kolejki, sprawdza, czy
bufor używany jako `SRC` na ustawiną flagę `interlock` na 1. Jeśli tak, to 
znaczy, że jakieś polecenie `INTERLOCK` zostało wysłane pomiędzy zapisem,
a odczytem z tej ramki. W przeciwnym przypadku, wysyłamy do kolejki `INTERLOCK`
i przechodzimy po liście wszystkich buforów, ustawiając jednocześnie flagę
`interlock`. 


## Kompilacja

Standardowo:
```
make KDIR=/path/to/kernel/build/dir
```

Moduł `harddoom.ko` powinien pojawić się w głównym katalogu.


