#include <linux/uaccess.h>
#include "structs/hd_pages.h"
#include "structs/hd_device.h"
#include "hd_conf.h"
#include "hd_common.h"
#include "hd_utils.h"

/*      Memory pages operations         */
/* Allocates DMA-available coherent memory page which params 
 * are defined by `dma_pool_create` call. `align` argument is
 * used to check DMA address.
 * Returns zero-filled memory space.*/
static int _hd_alloc_page_generic(struct dma_pool *pool, struct hd_page *page, uint32_t align)
{
        page->cpu_addr = dma_pool_zalloc(pool, GFP_KERNEL, &page->dma_addr);
        if (unlikely(page->cpu_addr == NULL))
                return -ENOMEM;
        BUG_ON(page->dma_addr & (align - 1));
        return 0;
}

void * hd_alloc_page2(struct hd_device *hd_dev, dma_addr_t *dma_addr)
{
        long res;
        struct hd_page page;
        res = _hd_alloc_page_generic(hd_dev->full_dma_pool, &page, HD_PAGE_SIZE);
        if (IS_ERR_VALUE(res))
                return ERR_PTR(res);
        *dma_addr = page.dma_addr;
        return page.cpu_addr;
}

int hd_alloc_page(struct hd_device *hd_dev, struct hd_page *page)
{
        return _hd_alloc_page_generic(hd_dev->full_dma_pool, page, HD_PAGE_SIZE);
}

int hd_alloc_cmap(struct hd_device *hd_dev, struct hd_page *page)
{
        return _hd_alloc_page_generic(hd_dev->cmap_dma_pool, page, HD_CMAP_SIZE);
}

void hd_free_page(struct hd_device *hd_dev, struct hd_page *page)
{
        hd_free_page2(hd_dev, page->cpu_addr, page->dma_addr);
}

void hd_free_page2(struct hd_device *hd_dev, void * cpu_addr, dma_addr_t dma_addr)
{
        dma_pool_free(hd_dev->full_dma_pool, cpu_addr, dma_addr);
}

void hd_free_cmap(struct hd_device *hd_dev, struct hd_page *page)
{
        dma_pool_free(hd_dev->cmap_dma_pool, page->cpu_addr, page->dma_addr);
}

/*      Page table operations           */

int hd_alloc_pagetable(struct hd_device *hd_dev, struct hd_pagetable *pt, int pages_num)
{
        long res;
        int pages_cnt;
        pt->cpu_ents = hd_alloc_page2(hd_dev, &pt->dma_ents);
        if (IS_ERR(pt->cpu_ents)) {
                res = PTR_ERR(pt->cpu_ents);
                HD_ERR("pagetable memory alloc failed!\n");
                goto err_pagetable_alloc;
        }
        for (pages_cnt = 0; pages_cnt < pages_num; pages_cnt++) {
                res = hd_alloc_page(hd_dev, pt->pages + pages_cnt);
                if (IS_ERR_VALUE(res)) {
                        HD_ERR("allocated %d out of %d pages and failed\n", pages_cnt, pages_num);
                        goto err_pages_alloc;
                }
                pt->cpu_ents[pages_cnt] = 
                        (pt->pages[pages_cnt].dma_addr & HD_PTE_MASK)
                        | HD_PTE_VALID;
        }
        return 0;

err_pages_alloc:
        hd_free_pagetable(hd_dev, pt);
err_pagetable_alloc:
        return res;
}

/* Pages allocations are zero filled so we can safely iterate 
 * through cpu_ents and check HD_PTE_VALID flag. */
void hd_free_pagetable(struct hd_device *hd_dev, struct hd_pagetable *pt)
{
        int i;
        for (i = 0; i < HD_PAGES_MAX; i++) {
                if (pt->cpu_ents[i] & HD_PTE_VALID)
                        hd_free_page(hd_dev, pt->pages + i);
        }
        hd_free_page2(hd_dev, pt->cpu_ents, pt->dma_ents);
}

long hd_copy_to_pages(struct hd_page *pages, void __user *from,
                unsigned long n, unsigned long offset, unsigned long page_sz)
{
        long copied;
        long res;
        int k;
        copied = 0;
        pages += offset / page_sz;
        offset -= page_sz * (offset / page_sz);
        while(n) {
                k = n + offset > page_sz ? page_sz - offset : n;
                res = copy_from_user(pages->cpu_addr + offset, from, k);
                if (res) {
                        res = -EFAULT;
                        break;
                }
                n -= k;
                copied += k;
                pages++;
                from += k;
                offset = 0;
        }
        return copied ? copied : res;
}

long hd_copy_from_pages(struct hd_page *pages, void __user *to,
                unsigned long n, unsigned long offset, unsigned long page_sz)
{
        long copied;
        long res;
        int k;
        copied = 0;
        k = offset / page_sz;
        pages += k;
        offset -= page_sz * (offset / page_sz);
        while(n) {
                k = n + offset > page_sz ? page_sz - offset : n;
                res = copy_to_user(to, pages->cpu_addr + offset, k);
                if (res) {
                        res = -EFAULT;
                        break;
                }
                n -= k;
                copied += k;
                pages++;
                to += k;
                offset = 0;
        }
        return copied ? copied : res;
}

