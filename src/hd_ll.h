/*
 * HardDoom™ low-level interface.
 */

#ifndef _HD_LL_H_
#define _HD_LL_H_

#include <linux/irq.h>
#include <linux/completion.h>
#include "structs/hd_device.h"
#include "structs/hd_resources.h"

irqreturn_t hd_irq(int irq, void *hd_dev);

int hd_ll_init(struct hd_device *hd_dev);

int hd_ll_restore(struct hd_device *hd_dev);


void hd_ll_reset(struct hd_device *hd_dev);

void hd_ll_start(struct hd_device *hd_dev);

void hd_ll_stop(struct hd_device *hd_dev);

void hd_ll_wait_sync(struct hd_device *hd_dev);

int hd_ll_wait_sync_interruptible(struct hd_device *hd_dev);

void hd_ll_set_cmd_buff_end(struct hd_queue *queue, dma_addr_t dma_addr);


uint32_t hd_ll_get_fence_last(struct hd_device *hd_dev);

void hd_ll_reset_fence_irq(struct hd_device *hd_dev);

void hd_ll_set_fence_wait(struct hd_device *hd_dev, uint32_t val);

#endif // _HD_LL_H_

