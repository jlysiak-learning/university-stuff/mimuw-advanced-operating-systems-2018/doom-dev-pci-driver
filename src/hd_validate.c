#include <linux/err.h>
#include <linux/file.h>
#include <linux/fs.h>

#include "hd_common.h"
#include "hd_validate.h"
#include "hd_limits.h"
#include "hd_res_ops.h"

int hd_validate_create_surface(struct doomdev_ioctl_create_surface *arg)
{
        if (arg->width < HD_CREATSURF_WIDTH_MIN)
                return -EINVAL;
        if (arg->width > HD_CREATSURF_WIDTH_MAX)
                return -EOVERFLOW;
        if (arg->width % HD_CREATSURF_WIDTH_MUL)
                return -EINVAL;
        if (arg->height < HD_CREATSURF_HEIGHT_MIN)
                return -EINVAL;
        if (arg->height > HD_CREATSURF_HEIGHT_MAX)
                return -EOVERFLOW;
        return 0;
}

int hd_validate_create_texture(struct doomdev_ioctl_create_texture *arg)
{
        if (arg->height > HD_CREATTEXT_HEIGHT_MAX)
                return -EOVERFLOW;
        if (arg->size > HD_CREATTEXT_SIZE_MAX)
                return -EOVERFLOW;
        return 0;
}

int hd_validate_create_colormaps(struct doomdev_ioctl_create_colormaps *arg)
{
        if (arg->num > HD_CREATCMAPS_NUM_MAX)
                return -EOVERFLOW;
        return 0;
}

int hd_validate_surf_copy_rects(struct doomdev_surf_ioctl_copy_rects *arg)
{
        long res;
        struct fd src_fd;
        res = 0;
        src_fd = fdget(arg->surf_src_fd);
        if (src_fd.file->f_op != &hd_res_surf_fops)
                res = -EINVAL;
        fdput(src_fd);
        return res;
}

int hd_validate_surf_copy_rect(struct hd_surface *src, struct hd_surface *dest,
                struct doomdev_copy_rect *arg)
{
        if (src->width != dest->width || src->height != dest->height)
                return -EINVAL;
        if (arg->pos_dst_x + arg->width > dest->width)
                return -EINVAL;
        if (arg->pos_dst_y + arg->height > dest->height)
                return -EINVAL;
        if (arg->pos_src_x + arg->width > src->width)
                return -EINVAL;
        if (arg->pos_src_y + arg->height > src->height)
                return -EINVAL;
        /* Solving R/W confilcts within one COPY_RECT are up to user. */
        return 0;
}

int hd_validate_surf_fill_rect(struct hd_surface *dest, 
                struct doomdev_fill_rect *arg)
{
        if (arg->pos_dst_x + arg->width > dest->width)
                return -EINVAL;
        if (arg->pos_dst_y + arg->height > dest->height)
                return -EINVAL;
        return 0;
}

int hd_validate_surf_draw_line(struct hd_surface * dest,
                struct doomdev_line *arg)
{
        if (arg->pos_a_x >= dest->width || arg->pos_a_y >= dest->height)
                return -EINVAL;
        if (arg->pos_b_x >= dest->width || arg->pos_b_y >= dest->height)
                return -EINVAL;
        return 0;
}

int hd_validate_surf_draw_background(struct doomdev_surf_ioctl_draw_background *arg)
{
        long res;
        struct fd src_fd;
        res = 0;
        src_fd = fdget(arg->flat_fd);
        if (src_fd.file->f_op != &hd_res_flat_fops) 
                res = -EINVAL;
        fdput(src_fd);
        return res;
}

int hd_validate_surf_draw_columns(struct doomdev_surf_ioctl_draw_columns * arg)
{
        struct fd src_fd;
        
        if (arg->draw_flags & (DOOMDEV_DRAW_FLAGS_FUZZ 
                                | DOOMDEV_DRAW_FLAGS_COLORMAP) ) {
                src_fd = fdget(arg->translations_fd);
                if (src_fd.file->f_op != &hd_res_cmaps_fops) {
                        fdput(src_fd);
                        return -EINVAL;
                }
                fdput(src_fd);
        }
        if (!(arg->draw_flags & DOOMDEV_DRAW_FLAGS_FUZZ) ) {
                src_fd = fdget(arg->texture_fd);
                if (src_fd.file->f_op != &hd_res_text_fops) {
                        fdput(src_fd);
                        return -EINVAL;
                }
                fdput(src_fd);
        }
        if (arg->draw_flags & DOOMDEV_DRAW_FLAGS_TRANSLATE) {
                src_fd = fdget(arg->colormaps_fd);
                if (src_fd.file->f_op != &hd_res_cmaps_fops) {
                        fdput(src_fd);
                        return -EINVAL;
                }
                fdput(src_fd);
        }
        if (arg->translation_idx >= HD_CREATCMAPS_NUM_MAX)
                return -EINVAL;
        return 0;
}

int hd_validate_surf_draw_column(struct hd_surface *dest, struct doomdev_column * arg)
{
        if (arg->colormap_idx >= HD_CREATCMAPS_NUM_MAX)
                return -EINVAL;
        if (arg->x >= dest->width)
                return -EINVAL;
        if (arg->y1 >= dest->height)
                return -EINVAL;
        if (arg->y2 >= dest->height)
                return -EINVAL;
        if (arg->ustart & ~0x03ffffff)
                return -EINVAL;
        if (arg->ustep & ~0x03ffffff)
                return -EINVAL;
        return 0;
}

int hd_validate_surf_draw_spans(struct doomdev_surf_ioctl_draw_spans * arg)
{
        struct fd src_fd;
        
        src_fd = fdget(arg->flat_fd);
        if (src_fd.file->f_op != &hd_res_flat_fops) {
                fdput(src_fd);
                return -EINVAL;
        }
        fdput(src_fd);
        if (arg->draw_flags & DOOMDEV_DRAW_FLAGS_TRANSLATE) {
                src_fd = fdget(arg->translations_fd);
                if (src_fd.file->f_op != &hd_res_cmaps_fops) {
                        fdput(src_fd);
                        return -EINVAL;
                }
                fdput(src_fd);
        }
        if (arg->draw_flags & DOOMDEV_DRAW_FLAGS_COLORMAP) {
                src_fd = fdget(arg->colormaps_fd);
                if (src_fd.file->f_op != &hd_res_cmaps_fops) {
                        fdput(src_fd);
                        return -EINVAL;
                }
                fdput(src_fd);
        }
        if (arg->translation_idx >= HD_CREATCMAPS_NUM_MAX)
                return -EINVAL;
        return 0;
}

int hd_validate_surf_draw_span(struct hd_surface *dest, struct doomdev_span * arg)
{
        if (arg->colormap_idx >= HD_CREATCMAPS_NUM_MAX)
                return -EINVAL;
        if (arg->x1 >= dest->width)
                return -EINVAL;
        if (arg->x2 >= dest->width)
                return -EINVAL;
        if (arg->y >= dest->height)
                return -EINVAL;
        return 0;
}
