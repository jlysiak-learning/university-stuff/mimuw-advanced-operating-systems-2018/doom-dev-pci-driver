/*
 * HardDoom™ device context management.
 */

#include "ext/harddoom.h"
#include "hd_queue.h"
#include "hd_ll.h"
#include "hd_conf.h"
#include "hd_common.h"



/*              CONTEXT SETUP           */

void hd_ctx_reset(struct hd_queue *queue) 
{
        queue->ctx.surf_dst = 0;
        queue->ctx.surf_src = 0;
        queue->ctx.texture = 0;
        queue->ctx.flat_addr = 0;
        queue->ctx.width = 0;
        queue->ctx.height = 0;
        queue->ctx.text_sz = 0;
        queue->ctx.text_h = 0;
        hd_queue_push_cmd(queue, HARDDOOM_CMD_XY_A(0, 0));
        queue->ctx.x_a = 0;
        queue->ctx.y_a = 0;
        hd_queue_push_cmd(queue, HARDDOOM_CMD_XY_B(0, 0));
        queue->ctx.x_b = 0;
        queue->ctx.y_b = 0;
        hd_queue_push_cmd(queue, HARDDOOM_CMD_FILL_COLOR(0));
        queue->ctx.color = 0;
        hd_queue_push_cmd(queue, HARDDOOM_CMD_USTEP(0));
        queue->ctx.ustep = 0;
        hd_queue_push_cmd(queue, HARDDOOM_CMD_USTART(0));
        queue->ctx.ustart = 0;
        hd_queue_push_cmd(queue, HARDDOOM_CMD_VSTEP(0));
        queue->ctx.vstep = 0;
        hd_queue_push_cmd(queue, HARDDOOM_CMD_VSTART(0));
        queue->ctx.vstart = 0;
        queue->ctx.cmap_main = 0;
        queue->ctx.cmap_tran = 0;
        hd_queue_push_cmd(queue, HARDDOOM_CMD_DRAW_PARAMS(0));
        queue->ctx.flags = 0;
}

/* Restore device context. */
void hd_ctx_restore(struct hd_queue *queue)
{
        queue->ctx.surf_dst = 0;
        queue->ctx.surf_src = 0;
        queue->ctx.texture = 0;
        queue->ctx.flat_addr = 0;
        queue->ctx.width = 0;
        queue->ctx.height = 0;
        queue->ctx.text_sz = 0;
        queue->ctx.text_h = 0;
        queue->ctx.cmap_main = 0;
        queue->ctx.cmap_tran = 0;
        hd_queue_push_cmd(queue, HARDDOOM_CMD_XY_A(queue->ctx.x_a,
                                queue->ctx.y_a = 0));
        hd_queue_push_cmd(queue, HARDDOOM_CMD_XY_B(queue->ctx.x_b,
                                queue->ctx.y_b = 0));
        hd_queue_push_cmd(queue, HARDDOOM_CMD_FILL_COLOR(queue->ctx.color));
        hd_queue_push_cmd(queue, HARDDOOM_CMD_USTEP(queue->ctx.ustep));
        hd_queue_push_cmd(queue, HARDDOOM_CMD_USTART(queue->ctx.ustart));
        hd_queue_push_cmd(queue, HARDDOOM_CMD_VSTEP(queue->ctx.vstep));
        hd_queue_push_cmd(queue, HARDDOOM_CMD_VSTART(queue->ctx.vstart));
        hd_queue_push_cmd(queue, HARDDOOM_CMD_DRAW_PARAMS(queue->ctx.flags));
}


/* NOTE 1: ALL THESE FUNCTIONS ARE CALLED WITH COMMAND QUEUE MUTEX HELD 
 * FROM APPROPRIATE SURFACE OPRERATION METHOD.                               
 *
 * NOTE 2: ALL OF THESE SETS NEW VALUE ONLY IF IT'S REQUIRED                 */

void hd_ctx_set_dest_surf_pt(struct hd_queue *queue, struct hd_pagetable *pt)
{
        if (queue->ctx.surf_dst != pt->dma_ents) {
                hd_queue_push_cmd(queue, 
                        HARDDOOM_CMD_SURF_DST_PT(pt->dma_ents));
                queue->ctx.surf_dst = pt->dma_ents;
        }
}

void hd_ctx_set_src_surf_pt(struct hd_queue *queue, struct hd_pagetable *pt)
{
        if (queue->ctx.surf_src != pt->dma_ents) {
                hd_queue_push_cmd(queue, 
                        HARDDOOM_CMD_SURF_SRC_PT(pt->dma_ents));
                queue->ctx.surf_src = pt->dma_ents;
        }
}

void hd_ctx_set_text_pt(struct hd_queue *queue, struct hd_pagetable *pt)
{
        if (queue->ctx.texture != pt->dma_ents) {
                hd_queue_push_cmd(queue, 
                        HARDDOOM_CMD_TEXTURE_PT(pt->dma_ents));
                queue->ctx.texture = pt->dma_ents;
        }
}

void hd_ctx_set_flat(struct hd_queue *queue, dma_addr_t addr)
{
        if (queue->ctx.flat_addr != addr) {
                hd_queue_push_cmd(queue, 
                        HARDDOOM_CMD_FLAT_ADDR(addr));
                queue->ctx.flat_addr = addr;
        }
}

void hd_ctx_set_surf_dims(struct hd_queue *queue, int w, int h)
{
        if (queue->ctx.width != w || queue->ctx.height != h) {
                hd_queue_push_cmd(queue,
                        HARDDOOM_CMD_SURF_DIMS(w, h));
                queue->ctx.width = w;
                queue->ctx.height = h;
        }
}

void hd_ctx_set_text_dims(struct hd_queue *queue, uint32_t sz, uint16_t h) 
{
        if (queue->ctx.text_sz != sz || queue->ctx.text_h != h) {
                hd_queue_push_cmd(queue,
                        HARDDOOM_CMD_TEXTURE_DIMS(sz, h));
                queue->ctx.text_sz = sz;
                queue->ctx.text_h = h;
        }
}

void hd_ctx_set_xy_a(struct hd_queue *queue, uint16_t x, uint16_t y)
{
        if (queue->ctx.x_a != x || queue->ctx.y_a != y) {
                hd_queue_push_cmd(queue,
                        HARDDOOM_CMD_XY_A(x, y));
                queue->ctx.x_a = x;
                queue->ctx.y_a = y;
        }
}

void hd_ctx_set_xy_b(struct hd_queue *queue, uint16_t x, uint16_t y)
{
        if (queue->ctx.x_b != x || queue->ctx.y_b != y) {
                hd_queue_push_cmd(queue,
                        HARDDOOM_CMD_XY_B(x, y));
                queue->ctx.x_b = x;
                queue->ctx.y_b = y;
        }
}

void hd_ctx_set_color(struct hd_queue *queue, uint8_t color)
{
        if (queue->ctx.color != color) {
                hd_queue_push_cmd(queue,
                        HARDDOOM_CMD_FILL_COLOR(color));
                queue->ctx.color = color;
        }
}


void hd_ctx_set_ustep(struct hd_queue *queue, uint32_t val)
{
        if (queue->ctx.ustep != val) {
                hd_queue_push_cmd(queue,
                        HARDDOOM_CMD_USTEP(val));
                queue->ctx.ustep = val;
        }
}

void hd_ctx_set_ustart(struct hd_queue *queue, uint32_t val)
{
        if (queue->ctx.ustart != val) {
                hd_queue_push_cmd(queue,
                        HARDDOOM_CMD_USTART(val));
                queue->ctx.ustart = val;
        }
}
void hd_ctx_set_vstep(struct hd_queue *queue, uint32_t val)
{
        if (queue->ctx.vstep != val) {
                hd_queue_push_cmd(queue,
                        HARDDOOM_CMD_VSTEP(val));
                queue->ctx.vstep = val;
        }
}

void hd_ctx_set_vstart(struct hd_queue *queue, uint32_t val)
{
        if (queue->ctx.vstart != val) {
                hd_queue_push_cmd(queue,
                        HARDDOOM_CMD_VSTART(val));
                queue->ctx.vstart = val;
        }
}

void hd_ctx_set_cmap(struct hd_queue *queue, struct hd_page *cmaps, uint8_t idx)
{
        if (queue->ctx.cmap_main != cmaps[idx].dma_addr) {
                hd_queue_push_cmd(queue,
                        HARDDOOM_CMD_COLORMAP_ADDR(cmaps[idx].dma_addr));
                queue->ctx.cmap_main = cmaps[idx].dma_addr;
        }
}

void hd_ctx_set_trans_cmap(struct hd_queue *queue, struct hd_page *cmaps, uint8_t idx)
{
        if (queue->ctx.cmap_tran != cmaps[idx].dma_addr) {
                hd_queue_push_cmd(queue,
                        HARDDOOM_CMD_TRANSLATION_ADDR(cmaps[idx].dma_addr));
                queue->ctx.cmap_tran = cmaps[idx].dma_addr;
        }
}

void hd_ctx_set_draw_params(struct hd_queue *queue, uint8_t flags)
{
        if (queue->ctx.flags != flags) {
                hd_queue_push_cmd(queue,
                        HARDDOOM_CMD_DRAW_PARAMS(flags));
                queue->ctx.flags = flags;
        }

}
