#ifndef _HD_COMMON_H_
#define _HD_COMMON_H_

#include <linux/printk.h>

#define HD_TAG                  "[HardDoom]"

#define HD_INFO(fmt, ...) \
        printk(KERN_INFO HD_TAG "[%s:%4d]" fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__)

#define HD_WARN(fmt, ...) \
        printk(KERN_WARNING HD_TAG "[%s:%4d]" fmt,__FUNCTION__, __LINE__, ##__VA_ARGS__)

#define HD_ERR(fmt, ...) \
        printk(KERN_ERR HD_TAG "[%s:%4d]" fmt,__FUNCTION__, __LINE__, ##__VA_ARGS__)

#endif // _HD_COMMON_H_
