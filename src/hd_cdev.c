/*
 * HardDoomm™ device driver interface via chardev.
 */

#include <linux/uaccess.h>
#include "ext/doomdev.h"
#include "structs/hd_device.h"
#include "hd_resources.h"
#include "hd_cdev.h"
#include "hd_common.h"
#include "hd_validate.h"

/*      cdev operations         */

/* Find device main struct and add
 * as private data. Nothing more to do. 
 * Resources are created by ioctl calls. */
static int hd_cdev_open(struct inode *inode, struct file *file)
{
        struct cdev *cdev;
        struct hd_device *hd_dev;

        cdev = inode->i_cdev;
        if (unlikely(cdev == NULL))
                return -ENXIO;
        
        hd_dev = cdev_to_hddev(cdev);
        file->private_data = hd_dev;
        HD_INFO("Opening char dev of %s...", hd_name(hd_dev));
        return 0;
}

static int hd_cdev_release(struct inode *inode, struct file *file)
{
        struct hd_device *hd_dev;
        
        hd_dev = file->private_data;
        file->private_data = NULL;
        HD_INFO("Releasing char dev of: %s...", hd_name(hd_dev));
        return 0;
}

/*      cdev ioctls - creating resources        */

static long hd_cdev_create_surface(struct hd_device *hd_dev, unsigned long arg)
{
        long res;
        struct doomdev_ioctl_create_surface data;
        struct hd_resource *hd_res;
        struct hd_surface *hd_surf;

        if (copy_from_user(&data, (struct doomdev_ioctl_create_surface *) arg, sizeof(data)))
                return -EFAULT;
       
        res = hd_validate_create_surface(&data);
        if (IS_ERR_VALUE(res))
                return res;
        
        HD_INFO("Creating surface on : %s\n", hd_name(hd_dev));
        
        hd_surf = hd_res_create_surface(hd_dev, data.width, data.height);
        if (IS_ERR(hd_surf)) {
                res = PTR_ERR(hd_surf);
                goto err_create_surface;
        }

        hd_res = hd_res_create_resource(hd_dev, HD_RES_TYPE_SURF, hd_surf);
        if (IS_ERR(hd_res)) {
                res = PTR_ERR(hd_res);
                goto err_create_res;
        }
        
        spin_lock(&hd_dev->lock);
        list_add_tail(&hd_surf->surf_list, &hd_dev->surf_list_head);
        spin_unlock(&hd_dev->lock);
        
        HD_INFO("Surface `%s` created on %s!\n", res_name(hd_res), hd_name(hd_dev));
        return hd_res->fd;

err_create_res:
        hd_res_free_surface(hd_dev, hd_surf);
err_create_surface:
        return res;
}

static long hd_cdev_create_texture(struct hd_device *hd_dev, unsigned long arg)
{
        long res;
        struct doomdev_ioctl_create_texture data;
        struct hd_resource *hd_res;
        struct hd_texture *hd_text;

        if (copy_from_user(&data, (struct doomdev_ioctl_create_texture *) arg, sizeof(data)))
                return -EFAULT;
        res = hd_validate_create_texture(&data);
        if (IS_ERR_VALUE(res))
                return res;
        HD_INFO("Creating texture on: %s\n", hd_name(hd_dev));

        hd_text = hd_res_create_texture(hd_dev, data.data_ptr, data.size, data.height);
        if (IS_ERR(hd_text)) {
                res = PTR_ERR(hd_text);
                goto err_create_text;
        }
        hd_res = hd_res_create_resource(hd_dev, HD_RES_TYPE_TEXT, hd_text);
        if (IS_ERR(hd_res)) {
                res = PTR_ERR(hd_res);
                goto err_create_res;
        }
        HD_INFO("Texture `%s` created on %s!\n", res_name(hd_res), hd_name(hd_dev));
        return hd_res->fd;

err_create_res:
        hd_res_free_texture(hd_dev, hd_text);
err_create_text:
        return res;
}

static long hd_cdev_create_flat(struct hd_device *hd_dev, unsigned long arg)
{
        long res;
        struct doomdev_ioctl_create_flat data;
        struct hd_resource *hd_res;
        struct hd_flat *hd_flat;

        if (copy_from_user(&data, (struct doomdev_ioctl_create_flat *) arg, sizeof(data)))
                return -EFAULT;
        HD_INFO("Creating flat texture on: %s\n", hd_name(hd_dev));
        
        hd_flat = hd_res_create_flat(hd_dev, data.data_ptr);
        if (IS_ERR(hd_flat)) {
                res = PTR_ERR(hd_flat);
                goto err_create_flat;
        }
        hd_res = hd_res_create_resource(hd_dev, HD_RES_TYPE_FLAT, hd_flat);
        if (IS_ERR(hd_res)) {
                res = PTR_ERR(hd_res);
                goto err_create_res;
        }
        HD_INFO("Flat texture `%s` created on %s!\n", res_name(hd_res), hd_name(hd_dev));
        return hd_res->fd;

err_create_res:
        hd_res_free_flat(hd_dev, hd_flat);
err_create_flat:
        return res;
}

static long hd_cdev_create_colormaps(struct hd_device *hd_dev, unsigned long arg)
{
        long res;
        struct doomdev_ioctl_create_colormaps data;
        struct hd_resource *hd_res;
        struct hd_cmap *hd_cmap;

        if (copy_from_user(&data, (struct doomdev_ioctl_create_colormaps *) arg, sizeof(data)))
                return -EFAULT;
        res = hd_validate_create_colormaps(&data);
        if (IS_ERR_VALUE(res))
                return res;
        
        HD_INFO("Creating color maps on: %s\n", hd_name(hd_dev));
        
        hd_cmap = hd_res_create_cmap(hd_dev, data.data_ptr, data.num);
        if (IS_ERR(hd_cmap)) {
                res = PTR_ERR(hd_cmap);
                goto err_create_cmap;
        }
        hd_res = hd_res_create_resource(hd_dev, HD_RES_TYPE_CMAP, hd_cmap);
        if (IS_ERR(hd_res)) {
                res = PTR_ERR(hd_res);
                goto err_create_res;
        }
        HD_INFO("Color map `%s` created on %s\n", res_name(hd_res), hd_name(hd_dev));
        return hd_res->fd;

err_create_res:
        hd_res_free_cmap(hd_dev, hd_cmap);
err_create_cmap:
        return res;
}

static long hd_cdev_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
        struct hd_device *hd_dev;
        long res;
        hd_dev = file->private_data;
        
        BUG_ON(hd_dev == NULL);

        switch (cmd) {
                case DOOMDEV_IOCTL_CREATE_SURFACE:
                        res = hd_cdev_create_surface(hd_dev,  arg);
                        break;
                case DOOMDEV_IOCTL_CREATE_TEXTURE:
                        res = hd_cdev_create_texture(hd_dev,  arg);
                        break;
                case DOOMDEV_IOCTL_CREATE_FLAT:
                        res = hd_cdev_create_flat(hd_dev, arg);
                        break;
                case DOOMDEV_IOCTL_CREATE_COLORMAPS:
                        res = hd_cdev_create_colormaps(hd_dev, arg);
                        break;
                default:
                        HD_WARN("Unrecognized command!\n");
                        res = -ENOTTY;
        } 
        return res;
}

struct file_operations hd_cdev_fops = {
        .owner = THIS_MODULE,
        .open = hd_cdev_open,
        .release = hd_cdev_release,
        .unlocked_ioctl = hd_cdev_ioctl,
        .compat_ioctl = hd_cdev_ioctl
};
