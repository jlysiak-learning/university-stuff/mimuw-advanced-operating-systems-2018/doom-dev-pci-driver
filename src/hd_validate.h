/*
 * Agruments validation methods.
 */
#ifndef _HD_VALIDATE_H_
#define _HD_VALIDATE_H_

#include "ext/doomdev.h"
#include "structs/hd_resources.h"

/*      Agrs for /dev/doom* ioclts */

int hd_validate_create_surface(struct doomdev_ioctl_create_surface *arg); 
int hd_validate_create_texture(struct doomdev_ioctl_create_texture *arg); 
int hd_validate_create_colormaps(struct doomdev_ioctl_create_colormaps *arg);

/*      COPY_RECTS validation */

int hd_validate_surf_copy_rects(struct doomdev_surf_ioctl_copy_rects *arg);
int hd_validate_surf_copy_rect(struct hd_surface *src, struct hd_surface * dest, 
                struct doomdev_copy_rect *arg);

/*      FILL_RECTS validation */

int hd_validate_surf_fill_rect(struct hd_surface * dest, 
                struct doomdev_fill_rect *arg);

/*      DRAW_LINE validation */

int hd_validate_surf_draw_line(struct hd_surface * dest,
                struct doomdev_line *arg);

/*      DRAW_BACKGROUND validation */

int hd_validate_surf_draw_background(struct doomdev_surf_ioctl_draw_background *arg);

/*      DRAW_COLUMN validation */

int hd_validate_surf_draw_columns(struct doomdev_surf_ioctl_draw_columns * arg);
int hd_validate_surf_draw_column(struct hd_surface *dest, struct doomdev_column * arg);

/*      DRAW_SPAN validation */

int hd_validate_surf_draw_spans(struct doomdev_surf_ioctl_draw_spans * arg);
int hd_validate_surf_draw_span(struct hd_surface *dest, struct doomdev_span * arg);

#endif // _HD_VALIDATE_H_
