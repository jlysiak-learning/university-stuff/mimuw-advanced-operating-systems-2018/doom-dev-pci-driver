/*
 * HardDoom™ low-level interface.
 * Here should be stored all MMIO operations.
 */


#include <linux/types.h>
#include <linux/err.h>
#include <linux/completion.h>
#include <linux/spinlock.h>

#include "ext/doomcode.h"
#include "ext/harddoom.h"
#include "hd_queue.h"
#include "hd_ll.h"
#include "hd_conf.h"
#include "hd_common.h"
#include "hd_utils.h"

uint32_t hd_ll_get_fence_last(struct hd_device *hd_dev)
{
        return ioread32(hd_dev->bar + HARDDOOM_FENCE_LAST);
}

void hd_ll_set_fence_wait(struct hd_device *hd_dev, uint32_t val)
{
        iowrite32(val, hd_dev->bar + HARDDOOM_FENCE_WAIT);
}

void hd_ll_reset_fence_irq(struct hd_device *hd_dev)
{
        iowrite32(HARDDOOM_INTR_FENCE, hd_dev->bar + HARDDOOM_INTR);
}


/* Write new value of CMD_WRITE_PTR register. */
void hd_ll_set_cmd_buff_end(struct hd_queue *queue, dma_addr_t dma_addr)
{
        struct hd_device *hd_dev = container_of(queue, struct hd_device, queue);
        iowrite32(dma_addr, hd_dev->bar + HARDDOOM_CMD_WRITE_PTR);
}

static void hd_ll_irq_fault(struct hd_device *hd_dev, uint32_t intr)
{
        if (intr & HARDDOOM_INTR_FE_ERROR) {
                HD_ERR("FETCH BLOCK ERROR!\n");
        }
        if (intr & HARDDOOM_INTR_FIFO_OVERFLOW) {
                HD_ERR("FIFO OVERFLOW ERROR!\n");
        }
        if (intr & HARDDOOM_INTR_SURF_DST_OVERFLOW) {
                HD_ERR("DST SURFACE OVERFLOW ERROR!\n");
        }
        if (intr & HARDDOOM_INTR_SURF_SRC_OVERFLOW) {
                HD_ERR("SRC SURFACE OVERFLOW ERROR!\n");
        }
        if (intr & HARDDOOM_INTR_PAGE_FAULT_SURF_DST) {
                HD_ERR("DST SURFACE PAGE FAULT ERROR!\n");
        }
        if (intr & HARDDOOM_INTR_PAGE_FAULT_SURF_SRC) {
                HD_ERR("SRC SURFACE PAGE FAULT ERROR!\n");
        }
        if (intr & HARDDOOM_INTR_PAGE_FAULT_TEXTURE) {
                HD_ERR("TEXTURE PAGE FAULT ERROR!\n");
        }
}

irqreturn_t hd_irq(int irq, void *hd_device)
{
        uint32_t intr;
        struct hd_device *hd_dev;
        
        hd_dev = (struct hd_device *) hd_device;
        intr = ioread32(hd_dev->bar + HARDDOOM_INTR);
        if (!intr)
                return IRQ_NONE;
        iowrite32(intr, hd_dev->bar + HARDDOOM_INTR);

        if (intr & HARDDOOM_INTR_PONG_ASYNC)
                hd_queue_irq_free_space_event(&hd_dev->queue);
        if (intr & HARDDOOM_INTR_FENCE)
                hd_queue_irq_fence(&hd_dev->queue);
        if (intr & HD_LL_INTR_FAULTS)
                hd_ll_irq_fault(hd_dev, intr);
        return IRQ_HANDLED;
}

void hd_ll_ucode_upload(struct hd_device *hd_dev)
{
        int i;
        int sz = sizeof(doomcode) / sizeof(* doomcode);
        HD_INFO("CODE SIZE: %d\n", sz);
        iowrite32(0, hd_dev->bar + HARDDOOM_FE_CODE_ADDR);
        for (i = 0; i < sz; ++i)
                iowrite32(doomcode[i], hd_dev->bar + HARDDOOM_FE_CODE_WINDOW);
}

/* Upload code and reset device.
 * Things done before DMA initialization. */
void hd_ll_reset(struct hd_device *hd_dev)
{
        hd_ll_ucode_upload(hd_dev);
        iowrite32(HARDDOOM_RESET_ALL, hd_dev->bar + HARDDOOM_RESET);
}

/* Finalize initialization.
 * NOTE: DMA must be initialized */
int hd_ll_init(struct hd_device *hd_dev)
{
        long res;
        struct hd_page page;

        spin_lock_init(&hd_dev->lock);
        sema_init(&hd_dev->sem_remove, 0);
        INIT_LIST_HEAD(&hd_dev->surf_list_head);

        res = hd_alloc_page(hd_dev, &page);
        if (IS_ERR_VALUE(res))
                return res;
        hd_queue_init(&hd_dev->queue, &page);

        iowrite32(hd_dev->queue.buff.dma_addr, hd_dev->bar + HARDDOOM_CMD_READ_PTR);
        iowrite32(hd_dev->queue.buff.dma_addr, hd_dev->bar + HARDDOOM_CMD_WRITE_PTR);
        
        iowrite32(HARDDOOM_INTR_MASK, hd_dev->bar + HARDDOOM_INTR);
        iowrite32(HARDDOOM_INTR_MASK, hd_dev->bar + HARDDOOM_INTR_ENABLE);
        iowrite32(0, hd_dev->bar + HARDDOOM_FENCE_WAIT);
        iowrite32(0, hd_dev->bar + HARDDOOM_FENCE_LAST);
        return 0;
}

void hd_ll_start(struct hd_device *hd_dev)
{
        iowrite32(HARDDOOM_ENABLE_ALL, hd_dev->bar + HARDDOOM_ENABLE);
}

void hd_ll_stop(struct hd_device *hd_dev)
{
        iowrite32(0, hd_dev->bar + HARDDOOM_ENABLE);
        iowrite32(0, hd_dev->bar + HARDDOOM_INTR_ENABLE);
        ioread32(hd_dev->bar + HARDDOOM_INTR_ENABLE);
}

int hd_ll_restore(struct hd_device *hd_dev)
{
        hd_dev->queue.b = 0;
        hd_dev->queue.l = 0;

        hd_dev->queue.async_counter = HD_QUEUE_ASYNC_IVAL;
        hd_dev->queue.fence_sent = 0;
        hd_dev->queue.fence_pending = 0;
        hd_dev->queue.fence_last = 0;
        iowrite32(hd_dev->queue.buff.dma_addr, hd_dev->bar + HARDDOOM_CMD_READ_PTR);
        iowrite32(hd_dev->queue.buff.dma_addr, hd_dev->bar + HARDDOOM_CMD_WRITE_PTR);
        iowrite32(HARDDOOM_INTR_MASK, hd_dev->bar + HARDDOOM_INTR);
        iowrite32(HARDDOOM_INTR_MASK, hd_dev->bar + HARDDOOM_INTR_ENABLE);
        iowrite32(0, hd_dev->bar + HARDDOOM_FENCE_WAIT);
        iowrite32(0, hd_dev->bar + HARDDOOM_FENCE_LAST);
        return 0;
}
