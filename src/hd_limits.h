#ifndef _HD_LIMITS_H_
#define _HD_LIMITS_H_

#define HD_CREATSURF_WIDTH_MIN          64
#define HD_CREATSURF_WIDTH_MAX          2048
#define HD_CREATSURF_WIDTH_MUL          64
#define HD_CREATSURF_HEIGHT_MIN         1
#define HD_CREATSURF_HEIGHT_MAX         2048

#define HD_CREATTEXT_HEIGHT_MAX         1023
#define HD_CREATTEXT_SIZE_MAX           0x400000

#define HD_CREATFLAT_SIZE               0x1000

#define HD_CREATCMAPS_NUM_MAX           0x100

#endif // _HD_LIMITS_H_
