#include "ext/harddoom.h"

#define HD_PCI_DRV_NAME         "harddoom"
#define HD_DEV_NAME             "doom"
#define HD_CDEV_NAME            "doom_cdev"

#define HD_DEV_MAX_CNT          256
#define HD_DEV_MMIO_SZ          4096

#define HD_DMA_BMASK            DMA_BIT_MASK(32)

#define HD_PAGE_SIZE            HARDDOOM_PAGE_SIZE
#define HD_PAGES_MAX            (HD_PAGE_SIZE / 4)
#define HD_CMAP_SIZE            HARDDOOM_COLORMAP_SIZE
#define HD_CMAPS_MAX            0x100

#define HD_PTE_VALID            HARDDOOM_PTE_VALID
#define HD_PTE_MASK             HARDDOOM_PTE_PHYS_MASK

#define HD_QUEUE_SZ             (HD_PAGE_SIZE / 4)              // 1024
#define HD_QUEUE_ASYNC_IVAL     (HD_QUEUE_SZ / 8)               // 128
#define HD_QUEUE_MIN_FREE_SPACE (HD_QUEUE_ASYNC_IVAL / 4)       // 32
#define HD_QUEUE_FENCE_MAX      HD_QUEUE_SZ

#define HD_LL_INTR_USED     \
        (HARDDOOM_INTR_FENCE | HARDDOOM_INTR_PONG_ASYNC)
#define HD_LL_INTR_FAULTS \
        (HARDDOOM_INTR_MASK & ~HD_LL_INTR_USED)
