/*
 * Handling HardDoom™ command queue
 */

#include "ext/harddoom.h"
#include "hd_queue.h"
#include "hd_ll.h"
#include "hd_conf.h"
#include "hd_common.h"

/*                      HELPERS                         */


/* NOTE: enter only is mutex is held and 
 * we have enough space in queue. */
static void _push_cmd(struct hd_queue *queue, uint32_t cmd)
{
        unsigned long flags;
        int e, ptr;
        uint32_t *buff;

        spin_lock_irqsave(&queue->internal_lock, flags);
        e = (queue->b + queue->l) % (HD_QUEUE_SZ - 1);
        ptr = (queue->b + queue->l + 1) % (HD_QUEUE_SZ - 1);
        buff = (uint32_t *) queue->buff.cpu_addr;
        buff[e] = cmd;
        queue->l++;
        hd_ll_set_cmd_buff_end(queue, queue->buff.dma_addr + 4 * ptr);
        spin_unlock_irqrestore(&queue->internal_lock, flags);
}

/* SPINLOCK MUST BE HELD! */
static int _get_free_space(struct hd_queue * queue)
{
        return HD_QUEUE_SZ - queue->l - 1;
}

/* SPINLOCK MUST BE HELD! */
static int _wait_again(struct hd_queue *queue, uint32_t wait_for)
{
        if (queue->fence_sent >= queue->fence_last) {
                if (wait_for > queue->fence_last)
                        return 1;
        } else { // send < last
                if (wait_for <= queue->fence_sent || 
                                        queue->fence_last < wait_for)
                        return 1;
        }
        return 0;
}

/* Mutex must be initialy held! 
 * Holding mutex guarantees that there's no
 * race between user processes. 
 * Only ASYNC IRQ can move queue pointers ahead. */
static int _wait_space_interruptible(struct hd_queue *queue)
{
        int space;
        long res;
        unsigned long flags;
        
        spin_lock_irqsave(&queue->internal_lock, flags);
        space = _get_free_space(queue);
        while (space < HD_QUEUE_MIN_FREE_SPACE) {
                spin_unlock_irqrestore(&queue->internal_lock, flags);
                res = wait_event_interruptible(queue->space_waiters, 
                        _get_free_space(queue) >= HD_QUEUE_MIN_FREE_SPACE);
                if (IS_ERR_VALUE(res))
                        return res;
                spin_lock_irqsave(&queue->internal_lock, flags); 
                space = _get_free_space(queue);
        }
        spin_unlock_irqrestore(&queue->internal_lock, flags);
        return 0;
}

/* Mutex must be initialy held! 
 * Holding mutex guarantees that there's no
 * race between user processes. 
 * Only ASYNC IRQ can move queue pointers ahead. */
static void _wait_space(struct hd_queue *queue)
{
        int space;
        unsigned long flags;
        
        spin_lock_irqsave(&queue->internal_lock, flags);
        space = _get_free_space(queue);
        while (space < HD_QUEUE_MIN_FREE_SPACE) {
                spin_unlock_irqrestore(&queue->internal_lock, flags);
                wait_event(queue->space_waiters, 
                        _get_free_space(queue) >= HD_QUEUE_MIN_FREE_SPACE);
                spin_lock_irqsave(&queue->internal_lock, flags); 
                space = _get_free_space(queue);
        }
        spin_unlock_irqrestore(&queue->internal_lock, flags);
}

/*                      INTERFACE                       */


/* Initialize structure using allocatted DMA-able page. */
void hd_queue_init(struct hd_queue *queue, struct hd_page *page)
{
        queue->b = 0;
        queue->l = 0;

        queue->fence_sent = 0;
        queue->fence_pending = 0;
        queue->fence_last = 0;
        init_waitqueue_head(&queue->fence_waiters);
        init_waitqueue_head(&queue->space_waiters);

        queue->async_counter = HD_QUEUE_ASYNC_IVAL;
        mutex_init(&queue->lock);
        spin_lock_init(&queue->internal_lock);
        queue->buff.cpu_addr = page->cpu_addr;
        queue->buff.dma_addr = page->dma_addr;
        /* Create device ring-buffer */
        ((uint32_t *) queue->buff.cpu_addr)[HD_QUEUE_SZ - 1] = HARDDOOM_CMD_JUMP(queue->buff.dma_addr);
}

/* NOTE: use hd_queue_acquire first 
 * Pushes commnad into the queue. 
 * May sleep. 
 * Returns -ERESTARTSYS on signal interrupt. */ 
int hd_queue_push_cmd_interruptible(struct hd_queue *queue, uint32_t cmd)
{
        int res;
        res = 0;
        if (!queue->async_counter) {
                if ((res = _wait_space_interruptible(queue)))
                        goto err;
                _push_cmd(queue, HARDDOOM_CMD_PING_ASYNC);
                queue->async_counter = HD_QUEUE_ASYNC_IVAL;
                queue->async_counter--;
        }
        if ((res = _wait_space_interruptible(queue)))
                goto err;
        _push_cmd(queue, cmd);
        queue->async_counter--;
        return res;
err:
        return res;
}

void hd_queue_push_cmd(struct hd_queue *queue, uint32_t cmd)
{
        if (!queue->async_counter) {
                _wait_space(queue);
                _push_cmd(queue, HARDDOOM_CMD_PING_ASYNC);
                queue->async_counter = HD_QUEUE_ASYNC_IVAL;
                queue->async_counter--;
        }
        _wait_space(queue);
        _push_cmd(queue, cmd);
        queue->async_counter--;
}

/* Locks queue struct or waits interruptible.
 * Must be called before operations on queue.
 * Enqueues all ioctls and lets grouping
 * many commands to device. 
 * NOTE: Returns -ERESTARTSYS when interrupted by signal. */
int hd_queue_acquire_interruptible(struct hd_queue *queue)
{
        int ret;
        ret = mutex_lock_interruptible(&queue->lock);
        if (ret == -EINTR)
                ret = -ERESTARTSYS;
        return ret;
}

void hd_queue_acquire(struct hd_queue *queue)
{
        mutex_lock(&queue->lock);
}

void hd_queue_leave(struct hd_queue *queue)
{       
        mutex_unlock(&queue->lock);
}

/* Sends FENCE and waits interruptible until all
 * commands will be processed by device. 
 * NOTE: DO NOT acquire queue before. */
int hd_queue_sync_interruptible(struct hd_queue *queue)
{
        long ret;
        int wait_for;
        unsigned long flags;
        ret = 0;
        /* Take queue mutex to serialize FENCE requests.
         * No other process can change state inside.
         * Races with IRQs are resolved using spinlocks. */
        ret = hd_queue_acquire_interruptible(queue);
        if (IS_ERR_VALUE(ret))
                goto unlock;

        spin_lock_irqsave(&queue->internal_lock, flags);
        queue->fence_sent = (queue->fence_sent + 1) % HD_QUEUE_FENCE_MAX;
        wait_for = queue->fence_sent;
        spin_unlock_irqrestore(&queue->internal_lock, flags);
        /* Push might sleep waiting for space in queue. */
        ret = hd_queue_push_cmd_interruptible(queue, HARDDOOM_CMD_FENCE(wait_for));
        if (IS_ERR_VALUE(ret))
                goto unlock;

        spin_lock_irqsave(&queue->internal_lock, flags);
        /* If any FENCE request is pending, then
         * this request will be handled in next IRQ.
         * If not, we have to restart procedure. */
        if (queue->fence_last == queue->fence_pending) {
                hd_ll_reset_fence_irq(queue_to_hddev(queue));
                queue->fence_pending = wait_for;
                hd_ll_set_fence_wait(queue_to_hddev(queue), wait_for);
                queue->fence_last = hd_ll_get_fence_last(queue_to_hddev(queue));
                if (queue->fence_last == wait_for) {
                        /* It's done. Return. */
                        spin_unlock_irqrestore(&queue->internal_lock, flags);
                        goto unlock;
                }
                /* Waiting for interrupt... */
        }
        spin_unlock_irqrestore(&queue->internal_lock, flags);
        hd_queue_leave(queue);

        spin_lock_irqsave(&queue->internal_lock, flags);
        while (_wait_again(queue, wait_for)) {
                spin_unlock_irqrestore(&queue->internal_lock, flags);
                ret = wait_event_interruptible(queue->fence_waiters, !_wait_again(queue, wait_for));
                if (IS_ERR_VALUE(ret))
                        return ret;
                spin_lock_irqsave(&queue->internal_lock, flags);
        }
        spin_unlock_irqrestore(&queue->internal_lock, flags);
        return ret;
unlock:
        hd_queue_leave(queue);
        return ret;
}

/* Sends FENCE and waits uninterruptible until all
 * commands will be processed by device. 
 * NOTE: DO NOT acquire queue before. 
 * Almost same as hd_queue_sync_interruptible but not interrruptible... */
void hd_queue_sync(struct hd_queue *queue)
{
        int wait_for;
        unsigned long flags;

        hd_queue_acquire(queue);
        spin_lock_irqsave(&queue->internal_lock, flags);
        queue->fence_sent = (queue->fence_sent + 1) % HD_QUEUE_FENCE_MAX;
        wait_for = queue->fence_sent;
        spin_unlock_irqrestore(&queue->internal_lock, flags);
        hd_queue_push_cmd(queue, HARDDOOM_CMD_FENCE(wait_for));

        spin_lock_irqsave(&queue->internal_lock, flags);
        if (queue->fence_last == queue->fence_pending) {
                hd_ll_reset_fence_irq(queue_to_hddev(queue));
                queue->fence_pending = wait_for;
                hd_ll_set_fence_wait(queue_to_hddev(queue), wait_for);
                queue->fence_last = hd_ll_get_fence_last(queue_to_hddev(queue));
                if (queue->fence_last == wait_for) {
                        spin_unlock_irqrestore(&queue->internal_lock, flags);
                        hd_queue_leave(queue);
                        return;
                }
        }
        spin_unlock_irqrestore(&queue->internal_lock, flags);
        hd_queue_leave(queue);

        spin_lock_irqsave(&queue->internal_lock, flags);
        while (_wait_again(queue, wait_for)) {
                spin_unlock_irqrestore(&queue->internal_lock, flags);
                wait_event(queue->fence_waiters, !_wait_again(queue, wait_for));
                spin_lock_irqsave(&queue->internal_lock, flags);
        }
        spin_unlock_irqrestore(&queue->internal_lock, flags);
}


/* Method used ONLY! in PING_ASYNC interrupt handler 
 * This function only moves queue begining. */
void hd_queue_irq_free_space_event(struct hd_queue *queue)
{
        unsigned long flags;
        spin_lock_irqsave(&queue->internal_lock, flags);
        queue->b = (queue->b + HD_QUEUE_ASYNC_IVAL) % (HD_QUEUE_SZ - 1);
        queue->l -= HD_QUEUE_ASYNC_IVAL;
        /* Wake up any sleeper waiting for space */
        wake_up_all(&queue->space_waiters);
        spin_unlock_irqrestore(&queue->internal_lock, flags);
}

/* FENCE IRQ method used ONLY in interrupt handler.
 * Moves pointers and starts new FENCE WAIT*/
void hd_queue_irq_fence(struct hd_queue *queue) 
{
        unsigned long flags;
        spin_lock_irqsave(&queue->internal_lock, flags);

        queue->fence_last = hd_ll_get_fence_last(queue_to_hddev(queue));
        /* Here we DONT HAVE to read FENCE_LAST. 
         * LAST should be equal pending */
        if (queue->fence_last != queue->fence_pending) {
                HD_ERR("WTF => FENCE != PENDING AT FENCE IRQ ENTRY\n");
        }
        
        /* Check what value of fence was requested 
         * If any FENCE request was sent in meantime set
         * new value of FENCE_WAIT.*/
        if (queue->fence_pending != queue->fence_sent) {
                /* Do not set INTR again, it was cleared 
                 * at the begining of IRQ. */
                queue->fence_pending = queue->fence_sent;
                hd_ll_set_fence_wait(queue_to_hddev(queue), 
                                queue->fence_pending);
        }

        /* Update value of LAST. */
        queue->fence_last = hd_ll_get_fence_last(queue_to_hddev(queue));

        /* Wake up all processes waiting for sync. */
        wake_up_all(&queue->fence_waiters);
        spin_unlock_irqrestore(&queue->internal_lock, flags);
}

