/*
 * HARDDOOM driver PCI subsystem operations.
 */

#ifndef _HD_PCI_H_
#define _HD_PCI_H_

#include <linux/pci.h>
#include <linux/device.h>

/*              PCI SUBSYSTEM STRUCTS           */

extern struct pci_driver hd_pci_driver;
extern dev_t hd_dev_major;
extern struct class hd_dev_class;

/*              PCI SUBSYSTEM OPERATIONS        */

int hd_probe(struct pci_dev *pci_dev, const struct pci_device_id *id);
void hd_remove(struct pci_dev *pci_dev);

int hd_suspend (struct pci_dev *pci_dev, pm_message_t state);
int hd_resume (struct pci_dev *pci_dev);

#endif //_HD_PCI_H_
