#include <linux/module.h>
#include <linux/uaccess.h>
#include <linux/fs.h>

#include "structs/hd_device.h"
#include "hd_resources.h"
#include "ext/doomdev.h"
#include "hd_res_ops.h"
#include "hd_surf.h"
#include "hd_common.h"
#include "hd_ll.h"
#include "hd_utils.h"
#include "hd_queue.h"

static int hd_res_release(struct inode *inode, struct file *file)
{
        struct hd_device *hd_dev;
        struct hd_resource * hd_res;

        hd_res = file->private_data;
        hd_dev = kobj_to_hddev(hd_res->kobj.parent);
        
        BUG_ON(hd_res == NULL);
        BUG_ON(hd_dev == NULL);
        BUG_ON(hd_res->resource == NULL);

        HD_INFO("Sending FENCE_RELEASE request for %s resource @ %s...", 
                        res_name(hd_res), kobject_name(&hd_dev->kobj));

        /* Here we are waiting uninterruptible.
         * If we cannot be woken up, can't free resources either. */
        hd_queue_sync(&hd_dev->queue); 

        HD_INFO("Synced! Freeing  %s resource @ %s...", 
                        res_name(hd_res), kobject_name(&hd_dev->kobj));
        /* Now do it safely! */
        switch (hd_res->type) {
                case HD_RES_TYPE_SURF:
                        hd_res_free_surface(hd_dev, 
                                        (struct hd_surface *) hd_res->resource);
                        break;
                case HD_RES_TYPE_TEXT:
                        hd_res_free_texture(hd_dev, 
                                        (struct hd_texture*) hd_res->resource);
                        break;
                case HD_RES_TYPE_FLAT:
                        hd_res_free_flat(hd_dev, 
                                        (struct hd_flat*) hd_res->resource);
                        break;
                case HD_RES_TYPE_CMAP:
                        hd_res_free_cmap(hd_dev, 
                                        (struct hd_cmap*) hd_res->resource);
                        break;
        }
        hd_res_free_resource(hd_res);
        return 0;
}

/* Surface ioctl commands dispatcher */
static long hd_res_surf_ioctl(struct file *file, 
                unsigned int cmd, unsigned long arg)
{
        struct hd_resource *hd_res;
        struct hd_device *hd_dev;
        long res;
        hd_res = (struct hd_resource *) file->private_data;
        hd_dev = kobj_to_hddev(hd_res->kobj.parent);

        switch (cmd) {
                case DOOMDEV_SURF_IOCTL_COPY_RECTS:
                        res = hd_surf_copy_rects(hd_dev, hd_res, arg);
                        break;
                case DOOMDEV_SURF_IOCTL_FILL_RECTS:
                        res = hd_surf_fill_rects(hd_dev, hd_res,arg);
                        break;
                case DOOMDEV_SURF_IOCTL_DRAW_LINES:
                        res = hd_surf_draw_lines(hd_dev, hd_res, arg);
                        break;
                case DOOMDEV_SURF_IOCTL_DRAW_BACKGROUND:
                        res = hd_surf_draw_background(hd_dev, hd_res, arg);
                        break;
                case DOOMDEV_SURF_IOCTL_DRAW_COLUMNS:
                        res = hd_surf_draw_columns(hd_dev, hd_res, arg);
                        break;
                case DOOMDEV_SURF_IOCTL_DRAW_SPANS:
                        res = hd_surf_draw_spans(hd_dev, hd_res, arg);
                        break;
                default:
                        HD_WARN("Unrecognized command!\n");
                        res = -ENOTTY;
        } 
        return res;
}

static ssize_t hd_res_surf_read(struct file *file, char __user *buff, size_t count, loff_t *offp)
{
        struct hd_device *hd_dev;
        struct hd_resource *hd_res;
        struct hd_surface *hd_surf;
        long res;
        loff_t pos;
        long cp;
        res = 0;
        cp = 0;
        hd_res = (struct hd_resource *) file->private_data;
        hd_dev = kobj_to_hddev(hd_res->kobj.parent);
        hd_surf = get_surface(hd_res);

        res = hd_surf_lock_interruptible(hd_surf); 
        if (IS_ERR_VALUE(res))
                return res;
        /* Check surface's dirty flag. Maybe it was not touched?
         * Especially, Doom game does reads in chunks of 640 bytes!
         * So, theres no sense in syncing every read syscall. */
        if (hd_surf->dirty) {
                res = hd_queue_sync_interruptible(&hd_dev->queue); 
                if (IS_ERR_VALUE(res))
                        goto unlock;
                /* After FENCE sync with surface mutex held,
                 * we know that no process pushes any command
                 * which could touch this surface. */
                hd_surf->dirty = 0;
        }

        pos = *offp;
        if (pos >= hd_surf->sz || pos < 0) {
                res = 0;
                goto unlock;
        }
        if (count > hd_surf->sz - pos)
                count = hd_surf->sz - pos;
        res = hd_copy_from_pages(hd_surf->pagetable.pages, buff, count, pos, HD_PAGE_SIZE);
        if (IS_ERR_VALUE(res))
                goto unlock;
        pos += res;
        *offp =  pos;
unlock:
        hd_surf_unlock(hd_surf);
        return res;
}

static loff_t hd_res_surf_llseek (struct file * file, loff_t off, int whence)
{
        struct hd_resource *hd_res;
        struct hd_surface *hd_surf;
        long res;
        hd_res = (struct hd_resource *) file->private_data;
        BUG_ON(hd_res == NULL);

        hd_surf = get_surface(hd_res);
        BUG_ON(hd_surf == NULL);

        res = hd_surf_lock_interruptible(hd_surf); 
        if (IS_ERR_VALUE(res))
                return res;

        switch (whence) {
                case SEEK_END:
                        file->f_pos = hd_surf->sz + off;
                        break;

                case SEEK_CUR:
                        file->f_pos += off;
                        break;

                case SEEK_SET:
                        file->f_pos = off;
                        break;
                default:
                        return -EINVAL;
        }
        hd_surf_unlock(hd_surf);
        return file->f_pos;
}

/*==== FILE OPERATIONS STRUCTS */
struct file_operations hd_res_text_fops = {
        .owner = THIS_MODULE,
        .release = hd_res_release
};

struct file_operations hd_res_flat_fops = {
        .owner = THIS_MODULE,
        .release = hd_res_release
};

struct file_operations hd_res_cmaps_fops = {
        .owner = THIS_MODULE,
        .release = hd_res_release
};


struct file_operations hd_res_surf_fops = {
        .owner = THIS_MODULE,
        .release = hd_res_release,
        .unlocked_ioctl = hd_res_surf_ioctl,
        .compat_ioctl = hd_res_surf_ioctl,
        .llseek = hd_res_surf_llseek,
        .read = hd_res_surf_read
};
