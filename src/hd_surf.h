/*
 * HardDoom™ surface resource operations.
 */

#ifndef _HD_SURF_H_
#define _HD_SURF_H_

#include "structs/hd_device.h"
#include "structs/hd_resources.h"


int hd_surf_lock_interruptible(struct hd_surface *surf);

void hd_surf_lock(struct hd_surface *surf);

void hd_surf_unlock(struct hd_surface *surf);

long hd_surf_copy_rects(struct hd_device *hd_dev, struct hd_resource *hd_res, 
                unsigned long arg);
long hd_surf_fill_rects(struct hd_device *hd_dev, struct hd_resource *hd_res,
                unsigned long arg);
long hd_surf_draw_lines(struct hd_device *hd_dev, struct hd_resource *hd_res,
                unsigned long arg);
long hd_surf_draw_background(struct hd_device *hd_dev, struct hd_resource *hd_res,
                unsigned long arg);
long hd_surf_draw_columns(struct hd_device *hd_dev, struct hd_resource *hd_res,
                unsigned long arg);
long hd_surf_draw_spans(struct hd_device *hd_dev, struct hd_resource *hd_res,
                unsigned long arg);

#endif // _HD_SURF_H_
