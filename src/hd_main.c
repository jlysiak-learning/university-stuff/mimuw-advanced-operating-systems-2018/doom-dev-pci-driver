/*
 * HARDDOOM driver module entry.
 */

#include <linux/module.h>
#include "hd_pci.h"
#include "hd_common.h"
#include "hd_conf.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Jacek Lysiak");
MODULE_DESCRIPTION("HardDoom(TM) device driver");


static int hd_init(void)
{
        long err;
        err = alloc_chrdev_region(&hd_dev_major, 0, HD_DEV_MAX_CNT, HD_DEV_NAME);
        if (IS_ERR_VALUE(err))
                goto err_alloc;
        err = class_register(&hd_dev_class);
        if (IS_ERR_VALUE(err))
                goto err_clss;
        err = pci_register_driver(&hd_pci_driver);
        if (IS_ERR_VALUE(err))
                goto err_pci_drv_reg;
        HD_INFO("Module initialized!");
        return 0;

err_pci_drv_reg:
        class_unregister(&hd_dev_class);
err_clss:
        unregister_chrdev_region(hd_dev_major, HD_DEV_MAX_CNT);
err_alloc:
        return err;
}

static void hd_exit(void)
{
        pci_unregister_driver(&hd_pci_driver);
        unregister_chrdev_region(hd_dev_major, HD_DEV_MAX_CNT);
        class_unregister(&hd_dev_class);
        HD_INFO("Module removed!");
}

module_init(hd_init);
module_exit(hd_exit);
