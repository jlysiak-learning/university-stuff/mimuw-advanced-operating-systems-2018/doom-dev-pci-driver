/*
 * HardDoom™ surface resource operations.
 */

#include <linux/uaccess.h>
#include <linux/file.h>
#include <linux/mutex.h>
#include "ext/doomdev.h"
#include "hd_resources.h"
#include "hd_surf.h"
#include "hd_validate.h"
#include "hd_common.h"
#include "hd_queue.h"
#include "hd_ctx.h"

/* Lock surface interruptible and return -ERESTARTSYS on interrupt */
int hd_surf_lock_interruptible(struct hd_surface *surf)
{
        long ret;
        ret = mutex_lock_interruptible(&surf->lock);
        if (ret == -EINTR)
                ret = -ERESTARTSYS;
        return ret;
}

void hd_surf_lock(struct hd_surface *surf)
{
        mutex_lock(&surf->lock);
}

void hd_surf_unlock(struct hd_surface *surf)
{
        mutex_unlock(&surf->lock);
}

long hd_surf_copy_rects(struct hd_device *hd_dev, struct hd_resource *hd_res, 
                unsigned long arg)
{
        struct doomdev_surf_ioctl_copy_rects rects;
        struct doomdev_copy_rect rect;
        struct hd_surface *src;
        struct hd_surface *dest;
        struct fd src_fd;
        uint16_t i;
        long res;

        if (copy_from_user(&rects, (struct doomdev_surf_ioctl_copy_rects *) arg, sizeof(rects)))
                return -EFAULT;

        res = hd_validate_surf_copy_rects(&rects);
        if (IS_ERR_VALUE(res))
                return res;

        src_fd = fdget(rects.surf_src_fd);
        src = get_surface((struct hd_resource *) src_fd.file->private_data);
        dest = get_surface(hd_res);

        /* Lock destination surface before write. 
         * Do not allow to read from this surface. */
        res = hd_surf_lock_interruptible(dest);
        if (IS_ERR_VALUE(res))
                goto err_surf;
        
        /* Acquire command queue or sleep if other ioctl is executed. */
        res = hd_queue_acquire_interruptible(&hd_dev->queue);
        if (IS_ERR_VALUE(res))
                goto err_acquire;

        /* All operations on interlock are done under queue mutex */
        if (src->interlock == 0) {
                /* There was no INTERLOCK before after accessing this
                 * surface. Send INTERLOCK and set to 1 in all surfaces. */
                struct list_head *tmp;
                struct hd_surface *surf;
                res = hd_queue_push_cmd_interruptible(&hd_dev->queue, 
                                HARDDOOM_CMD_INTERLOCK);
                if (IS_ERR_VALUE(res))
                        goto err;

                spin_lock(&hd_dev->lock);
                list_for_each(tmp, &hd_dev->surf_list_head) {
                        surf = list_entry(tmp, struct hd_surface, surf_list);
                        surf->interlock = 1;
                }
                spin_unlock(&hd_dev->lock);
        }
        
        hd_ctx_set_dest_surf_pt(&hd_dev->queue, &dest->pagetable);
        hd_ctx_set_src_surf_pt(&hd_dev->queue, &src->pagetable);
        hd_ctx_set_surf_dims(&hd_dev->queue, dest->width, dest->height);

        for (i = 0; i < rects.rects_num; ++i, rects.rects_ptr += sizeof(rect)) {
                if (copy_from_user(&rect, (struct doomdev_copy_rect *) rects.rects_ptr, sizeof(rect))) {
                        res = -EFAULT;
                        break;
                }
                res = hd_validate_surf_copy_rect(src, dest, &rect);
                if (IS_ERR_VALUE(res))
                        break;
                hd_ctx_set_xy_a(&hd_dev->queue, rect.pos_dst_x, rect.pos_dst_y);
                hd_ctx_set_xy_b(&hd_dev->queue, rect.pos_src_x, rect.pos_src_y);
                res = hd_queue_push_cmd_interruptible(&hd_dev->queue, 
                        HARDDOOM_CMD_COPY_RECT(rect.width, rect.height));
                if (IS_ERR_VALUE(res))
                        break;
        }
        
        if (i) {
                /* Mark as dirty and set interlock required 
                 * only if any operation was successful */
                dest->dirty = 1;
                dest->interlock = 0;
        }
        hd_queue_leave(&hd_dev->queue);
        hd_surf_unlock(dest);
        fdput(src_fd);
        return i ? i : res;

err:
        hd_queue_leave(&hd_dev->queue);
err_acquire:
        hd_surf_unlock(dest);
err_surf:
        fdput(src_fd);
        return res;
}

long hd_surf_fill_rects(struct hd_device *hd_dev, struct hd_resource *hd_res, 
                unsigned long arg)
{
        struct doomdev_surf_ioctl_fill_rects rects;
        struct doomdev_fill_rect rect;
        struct hd_surface *dest;
        uint16_t i;
        long res;

        if (copy_from_user(&rects, (struct doomdev_surf_ioctl_fill_rects *) arg, sizeof(rects)))
                return -EFAULT;
        
        dest = get_surface(hd_res);

        BUG_ON(dest == NULL);
        res = hd_surf_lock_interruptible(dest);
        if (IS_ERR_VALUE(res))
                return res;
        
        res = hd_queue_acquire_interruptible(&hd_dev->queue);
        if (IS_ERR_VALUE(res))
                goto err_acquire;
        
        hd_ctx_set_dest_surf_pt(&hd_dev->queue, &dest->pagetable);
        hd_ctx_set_surf_dims(&hd_dev->queue, dest->width, dest->height);

        for (i = 0; i < rects.rects_num; ++i, rects.rects_ptr += sizeof(rect)) {
                if (copy_from_user(&rect, (void *) rects.rects_ptr, sizeof(rect))) {
                        res = -EFAULT;
                        break;
                }
                res = hd_validate_surf_fill_rect(dest, &rect);
                if (IS_ERR_VALUE(res))
                        break;
                hd_ctx_set_xy_a(&hd_dev->queue, rect.pos_dst_x, rect.pos_dst_y);
                hd_ctx_set_color(&hd_dev->queue, rect.color);
                res = hd_queue_push_cmd_interruptible(&hd_dev->queue, 
                      HARDDOOM_CMD_FILL_RECT(rect.width, rect.height));
                if (IS_ERR_VALUE(res))
                        break;
        }

        if (i) {
                dest->dirty = 1;
                dest->interlock = 0;
        }

        hd_queue_leave(&hd_dev->queue);
        hd_surf_unlock(dest);
        return i ? i : res;

err_acquire:
        hd_surf_unlock(dest);
        return res;
}

long hd_surf_draw_lines(struct hd_device *hd_dev, struct hd_resource *hd_res, 
                unsigned long arg)
{
        struct doomdev_surf_ioctl_draw_lines lines;
        struct doomdev_line line;
        struct hd_surface *dest;
        uint16_t i;
        long res;
        
        if (copy_from_user(&lines, (struct doomdev_surf_ioctl_draw_lines *) arg, sizeof(lines)))
                return -EFAULT;
        
        dest = (struct hd_surface *) hd_res->resource;

        res = hd_surf_lock_interruptible(dest);
        if (IS_ERR_VALUE(res))
                return res;
        
        res = hd_queue_acquire_interruptible(&hd_dev->queue);
        if (IS_ERR_VALUE(res))
                goto err_acquire;

        hd_ctx_set_dest_surf_pt(&hd_dev->queue, &dest->pagetable); 
        hd_ctx_set_surf_dims(&hd_dev->queue, dest->width, dest->height);
               
        for (i = 0; i < lines.lines_num; ++i, lines.lines_ptr += sizeof(line)) {
                if (copy_from_user(&line, (void *) lines.lines_ptr, sizeof(line))) {
                        res = -EFAULT;
                        break;
                }
                res = hd_validate_surf_draw_line(dest, &line);
                if (IS_ERR_VALUE(res)) {
                        break;
                }

                hd_ctx_set_xy_a(&hd_dev->queue, line.pos_a_x, line.pos_a_y);
                hd_ctx_set_xy_b(&hd_dev->queue, line.pos_b_x, line.pos_b_y);
                hd_ctx_set_color(&hd_dev->queue, line.color);
                res = hd_queue_push_cmd_interruptible(&hd_dev->queue, 
                                HARDDOOM_CMD_DRAW_LINE);
                if (IS_ERR_VALUE(res))
                        break;
        }

        if (i) {
                dest->dirty = 1;
                dest->interlock = 0;
        }

        hd_queue_leave(&hd_dev->queue);
        hd_surf_unlock(dest);
        return i ? i : res;

err_acquire:
        hd_surf_unlock(dest);
        return res;
}

long hd_surf_draw_background(struct hd_device *hd_dev, struct hd_resource *hd_res,
                unsigned long arg)
{
        struct doomdev_surf_ioctl_draw_background bkg;
        struct hd_surface *dest;
        struct hd_flat *flat;
        struct fd src_fd;
        long res;

        if (copy_from_user(&bkg, (struct doomdev_surf_ioctl_draw_background *) arg, sizeof(bkg)))
                return -EFAULT;

        res = hd_validate_surf_draw_background(&bkg);
        if (IS_ERR_VALUE(res))
                return res;
        
        src_fd = fdget(bkg.flat_fd);
        flat = get_flat(((struct hd_resource *) src_fd.file->private_data));
        dest = get_surface(hd_res);

        res = hd_surf_lock_interruptible(dest);
        if (IS_ERR_VALUE(res))
                goto err_surf;
        
        /* Acquire command queue or sleep if other ioctl is executed. */
        res = hd_queue_acquire_interruptible(&hd_dev->queue);
        if (IS_ERR_VALUE(res))
                goto err_acquire;
        
        hd_ctx_set_dest_surf_pt(&hd_dev->queue, &dest->pagetable);
        hd_ctx_set_flat(&hd_dev->queue, flat->buffer.dma_addr);
        hd_ctx_set_surf_dims(&hd_dev->queue, dest->width, dest->height);
        
        res = hd_queue_push_cmd_interruptible(&hd_dev->queue, 
                        HARDDOOM_CMD_DRAW_BACKGROUND);
        if (IS_ERR_VALUE(res))
                goto err_acquire;

        dest->dirty = 1;
        dest->interlock = 0;
        
        hd_queue_leave(&hd_dev->queue);

err_acquire:
        hd_surf_unlock(dest);
err_surf:
        fdput(src_fd);
        return res;
}

long hd_surf_draw_columns(struct hd_device *hd_dev, struct hd_resource *hd_res, unsigned long arg)
{
        struct doomdev_surf_ioctl_draw_columns cols;
        struct doomdev_column col;
        struct hd_texture *text;
        struct hd_cmap *trans;
        struct hd_cmap *cmap;
        struct hd_surface *dest; 
        struct fd text_fd;
        struct fd cmap_fd;
        struct fd trans_fd; 
        uint16_t i;
        long res;
        
        i = 0;
        text = NULL;
        trans = NULL;
        cmap = NULL;

        if (copy_from_user(&cols, (struct doomdev_surf_ioctl_draw_columns *) arg, sizeof(cols)))
                return -EFAULT;
        res = hd_validate_surf_draw_columns(&cols); 
        if (IS_ERR_VALUE(res))
                return res;
        
        dest = get_surface(hd_res);
        
        if (cols.draw_flags & (DOOMDEV_DRAW_FLAGS_FUZZ 
                                | DOOMDEV_DRAW_FLAGS_COLORMAP) ) {
                cmap_fd = fdget(cols.colormaps_fd);
                cmap = get_cmap(((struct hd_resource *) cmap_fd.file->private_data));
        }
        if (!(cols.draw_flags & DOOMDEV_DRAW_FLAGS_FUZZ) ) {
                text_fd = fdget(cols.texture_fd);
                text = get_texture(((struct hd_resource *) text_fd.file->private_data));
        }
        if (cols.draw_flags & DOOMDEV_DRAW_FLAGS_TRANSLATE) {
                trans_fd = fdget(cols.translations_fd);
                trans = get_cmap(((struct hd_resource *) trans_fd.file->private_data));
        }
        
        res = hd_surf_lock_interruptible(dest);
        if (IS_ERR_VALUE(res))
                goto err_surf;
        
        res = hd_queue_acquire_interruptible(&hd_dev->queue);
        if (IS_ERR_VALUE(res))
                goto err_acquire;

        hd_ctx_set_dest_surf_pt(&hd_dev->queue, &dest->pagetable);
        hd_ctx_set_surf_dims(&hd_dev->queue, dest->width, dest->height);
        hd_ctx_set_draw_params(&hd_dev->queue, cols.draw_flags);

        if (!(cols.draw_flags & DOOMDEV_DRAW_FLAGS_FUZZ)) {
                BUG_ON(text == NULL);
                hd_ctx_set_text_pt(&hd_dev->queue, &text->pagetable);
                hd_ctx_set_text_dims(&hd_dev->queue, text->size, text->height);
        }
        if (cols.draw_flags & DOOMDEV_DRAW_FLAGS_TRANSLATE) {
                BUG_ON(trans == NULL);
                hd_ctx_set_trans_cmap(&hd_dev->queue, trans->cmaps, cols.translation_idx);
        }
      
        for (; i < cols.columns_num; ++i, cols.columns_ptr += sizeof(col)) {
                uint16_t y1, y2;

                if (copy_from_user(&col, (struct doomdev_column *) cols.columns_ptr, sizeof(col))) {
                        res = -EFAULT;
                        break;
                }
                res = hd_validate_surf_draw_column(dest, &col);
                if (IS_ERR_VALUE(res)){
                        break;
                }

                if (cols.draw_flags & (DOOMDEV_DRAW_FLAGS_FUZZ 
                                        | DOOMDEV_DRAW_FLAGS_COLORMAP) ) {
                        BUG_ON(cmap == NULL);
                        hd_ctx_set_cmap(&hd_dev->queue, cmap->cmaps, col.colormap_idx);
                }
                y1 = col.y1 > col.y2 ? col.y2 : col.y1;
                y2 = col.y1 > col.y2 ? col.y1 : col.y2;
                hd_ctx_set_xy_a(&hd_dev->queue, col.x, y1);
                hd_ctx_set_xy_b(&hd_dev->queue, col.x, y2);
                hd_ctx_set_ustep(&hd_dev->queue, col.ustep);
                
                if (!(cols.draw_flags & DOOMDEV_DRAW_FLAGS_FUZZ))
                        hd_ctx_set_ustart(&hd_dev->queue, col.ustart);
                res = hd_queue_push_cmd_interruptible(&hd_dev->queue, 
                                HARDDOOM_CMD_DRAW_COLUMN(col.texture_offset));
                if (IS_ERR_VALUE(res))
                        break;
        }
        
        if (i) {
                dest->dirty = 1;
                dest->interlock = 0;
        }
        
        hd_queue_leave(&hd_dev->queue);
err_acquire:
        hd_surf_unlock(dest);
err_surf: 
        if (cols.draw_flags & (DOOMDEV_DRAW_FLAGS_FUZZ 
                                | DOOMDEV_DRAW_FLAGS_COLORMAP) ) {
                BUG_ON(cmap == NULL);
                fdput(cmap_fd);
        }
        if (!(cols.draw_flags & DOOMDEV_DRAW_FLAGS_FUZZ) ) {
                BUG_ON(text == NULL);
                fdput(text_fd);
        }
        if (cols.draw_flags & DOOMDEV_DRAW_FLAGS_TRANSLATE) {
                BUG_ON(trans == NULL);
                fdput(trans_fd);
        }
        return i ? i : res;
}

long hd_surf_draw_spans(struct hd_device *hd_dev, struct hd_resource *hd_res, unsigned long arg)
{
        struct doomdev_surf_ioctl_draw_spans spans;
        struct doomdev_span span;
        uint16_t i;
        long res;
        
        struct hd_flat *flat;
        struct hd_cmap *trans;
        struct hd_cmap *cmap;
        struct hd_surface *dest;
        
        struct fd flat_fd;
        struct fd cmap_fd;
        struct fd trans_fd;
        
        i = 0;
        flat = NULL;
        trans = NULL;
        cmap = NULL;

        if (copy_from_user(&spans, (struct doomdev_surf_ioctl_draw_spans *) arg, sizeof(spans))) {
                return -EFAULT;
        }
        res = hd_validate_surf_draw_spans(&spans);
        if (IS_ERR_VALUE(res)) {
                return res;
        }
               
        dest = get_surface(hd_res);
        flat_fd = fdget(spans.flat_fd);
        flat = get_flat(((struct hd_resource *) flat_fd.file->private_data));
        
        if (spans.draw_flags & DOOMDEV_DRAW_FLAGS_COLORMAP) {
                cmap_fd = fdget(spans.colormaps_fd);
                cmap = get_cmap(((struct hd_resource *) cmap_fd.file->private_data));
        }
        if (spans.draw_flags & DOOMDEV_DRAW_FLAGS_TRANSLATE) {
                trans_fd = fdget(spans.translations_fd);
                trans = get_cmap(((struct hd_resource *) trans_fd.file->private_data));
        }
        
        res = hd_surf_lock_interruptible(dest);
        if (IS_ERR_VALUE(res)) {
                goto err_surf;
        }
        
        res = hd_queue_acquire_interruptible(&hd_dev->queue);
        if (IS_ERR_VALUE(res)) {
                goto err_acquire;
        }

        hd_ctx_set_dest_surf_pt(&hd_dev->queue, &dest->pagetable);
        hd_ctx_set_surf_dims(&hd_dev->queue, dest->width, dest->height);
        hd_ctx_set_flat(&hd_dev->queue, flat->buffer.dma_addr);
        hd_ctx_set_draw_params(&hd_dev->queue, spans.draw_flags);
        
        if (spans.draw_flags & DOOMDEV_DRAW_FLAGS_TRANSLATE) {
                BUG_ON(trans == NULL);
                hd_ctx_set_trans_cmap(&hd_dev->queue, trans->cmaps, spans.translation_idx);
        }

        for (i = 0; i < spans.spans_num; ++i, spans.spans_ptr += sizeof(span)) {
                uint16_t x1, x2;
                if (copy_from_user(&span, (void *) spans.spans_ptr, sizeof(span))) {
                        res = -EFAULT;
                        break;
                }
                res = hd_validate_surf_draw_span(dest, &span);
                if (IS_ERR_VALUE(res)) {
                        break;
                }
                if (spans.draw_flags & DOOMDEV_DRAW_FLAGS_COLORMAP) {
                        BUG_ON(cmap == NULL);
                        hd_ctx_set_cmap(&hd_dev->queue, cmap->cmaps, span.colormap_idx);
                }
                
                x1 = span.x1 > span.x2 ? span.x2 : span.x1;
                x2 = span.x1 > span.x2 ? span.x1 : span.x2;

                hd_ctx_set_xy_a(&hd_dev->queue, x1, span.y);
                hd_ctx_set_xy_b(&hd_dev->queue, x2, span.y);
                hd_ctx_set_ustep(&hd_dev->queue, span.ustep & 0x003fffff);
                hd_ctx_set_ustart(&hd_dev->queue, span.ustart & 0x003fffff);
                hd_ctx_set_vstep(&hd_dev->queue, span.vstep & 0x003fffff);
                hd_ctx_set_vstart(&hd_dev->queue, span.vstart & 0x003fffff);
                res = hd_queue_push_cmd_interruptible(&hd_dev->queue, 
                                HARDDOOM_CMD_DRAW_SPAN);
                if (IS_ERR_VALUE(res)) {
                        break;
                }
        }

        hd_queue_leave(&hd_dev->queue);
err_acquire:
        hd_surf_unlock(dest);
err_surf: 
        if (spans.draw_flags & DOOMDEV_DRAW_FLAGS_COLORMAP) {
                BUG_ON(cmap == NULL);
                fdput(cmap_fd);
        }
        if (spans.draw_flags & DOOMDEV_DRAW_FLAGS_TRANSLATE) {
                BUG_ON(trans == NULL);
                fdput(trans_fd);
        }
        fdput(flat_fd);
        return i ? i : res;
}
