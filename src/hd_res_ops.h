/*
 * Operations on created resources.
 */

#ifndef _HD_RES_OPS_H_
#define _HD_RES_OPS_H_

#include <linux/fs.h>

extern struct file_operations hd_res_text_fops;
extern struct file_operations hd_res_flat_fops;
extern struct file_operations hd_res_cmaps_fops;
extern struct file_operations hd_res_surf_fops;

#endif // _HD_RES_OPS_H_
