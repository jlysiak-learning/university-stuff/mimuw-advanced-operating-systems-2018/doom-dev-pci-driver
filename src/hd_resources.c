/*
 * HardDoom™ resources allocation and freeing.
 */

#include <linux/kernel.h>
#include <linux/kobject.h>
#include <linux/anon_inodes.h>
#include <linux/file.h>
#include "hd_resources.h"
#include "hd_res_ops.h"
#include "hd_common.h"
#include "hd_utils.h"

/*      kobject releases        */

static void hd_res_kobj_release(struct kobject *kobj)
{
}

static struct kobj_type hd_res_kobj_type = {
        .release = hd_res_kobj_release
};


struct hd_resource * hd_res_create_resource(struct hd_device *hd_dev,
                uint8_t type, void * resource)
{
        long res;
        unsigned long flags;
        struct hd_resource *hd_res;
        struct file_operations *f_ops;
        struct fd src_fd;
        
        BUG_ON(resource == NULL);

        f_ops = NULL;
        hd_res = kzalloc(sizeof(struct hd_resource), GFP_KERNEL);
        if (hd_res == NULL) {
                res = -ENOMEM;
                HD_ERR("hd_res_create_resource kzalloc failed!\n");
                goto err_kzalloc;
        }
        spin_lock_init(&hd_res->lock);

        spin_lock_irqsave(&hd_dev->lock, flags);
        /* Only `++` is required to be done under spinlock,
         * but I don't want to write next switch.
         *
         * I know that creating ID just by incrementing u32 is
         * in general not so good idea but should work well :) */
        switch (type) {
                case HD_RES_TYPE_SURF:
                        hd_res->idx = hd_dev->surf_num++;
                        sprintf(hd_res->name, "surf%u", hd_res->idx);
                        f_ops = &hd_res_surf_fops; 
                        break;
                case HD_RES_TYPE_TEXT:
                        hd_res->idx = hd_dev->text_num++;
                        sprintf(hd_res->name, "text%u", hd_res->idx);
                        f_ops = &hd_res_text_fops; 
                        break;
                case HD_RES_TYPE_FLAT:
                        hd_res->idx = hd_dev->flat_num++;
                        sprintf(hd_res->name, "flat%u", hd_res->idx);
                        f_ops = &hd_res_flat_fops; 
                        break;
                case HD_RES_TYPE_CMAP:
                        hd_res->idx = hd_dev->cmap_num++;
                        sprintf(hd_res->name, "cmap%u", hd_res->idx);
                        f_ops = &hd_res_cmaps_fops; 
                        break;
        }
        spin_unlock_irqrestore(&hd_dev->lock, flags);
        BUG_ON(f_ops == NULL);
        HD_INFO("Creating %s (type: %d)\n", hd_res->name, type);
        
        res = kobject_init_and_add(&hd_res->kobj, &hd_res_kobj_type, 
                        &hd_dev->kobj, hd_res->name);
        if (IS_ERR_VALUE(res)) {
                HD_ERR("resource `%s` kobject_init_and_add error %ld\n", 
                                hd_res->name, res);
                goto err_kobj_init_and_add;
        }
        
        res = anon_inode_getfd(hd_res->name, f_ops, hd_res, 
                        FMODE_LSEEK | FMODE_PREAD | FMODE_PWRITE);

        if (IS_ERR_VALUE(res)) {
                HD_ERR("resource `%s` anon_inode_getfile failed error %ld\n",
                                hd_res->name, res);
                goto err_anon_inode_getfd;
        }
        src_fd = fdget(res);
        src_fd.file->f_mode |= FMODE_LSEEK | FMODE_PREAD | FMODE_PWRITE;
        fdput(src_fd);
        hd_res->fd = res;
        BUG_ON(resource == NULL);
        hd_res->resource = resource;
        hd_res->type = type;

        return hd_res;

err_anon_inode_getfd:
        kobject_del(&hd_res->kobj);
        kobject_put(&hd_res->kobj);
err_kobj_init_and_add:
        kfree(hd_res);
err_kzalloc:
        return ERR_PTR(res);
}

/* This function is called within `hd_res_release` when:
 *  - user closed file descriptor to resource 
 *  - command queue sync mechanism send completion event and 
 *      we are sure that nobody will ever use this              
 *  - previously held content were freed.  */
void hd_res_free_resource(struct hd_resource *hd_res)
{
        /* Remove from hierarhy. 
         * Delete put parent's kobject.
         * Parent won't let close chardev until
         * any resource stays open. */
        kobject_del(&hd_res->kobj);
        kobject_put(&hd_res->kobj);
        kfree(hd_res);
}

struct hd_surface * hd_res_create_surface(struct hd_device *hd_dev,
                uint16_t w, uint16_t h)
{
        long res;
        struct hd_surface *hd_surf;
        int pages_num;

        hd_surf = kzalloc(sizeof(struct hd_surface), GFP_KERNEL);
        if (hd_surf == NULL) {
                res = -ENOMEM;
                HD_ERR("hd_surface kzalloc failed!\n");
                goto err_kzalloc;
        }
        
        pages_num = (w * h + HD_PAGE_SIZE - 1) / HD_PAGE_SIZE;
        res = hd_alloc_pagetable(hd_dev, &hd_surf->pagetable, pages_num);
        if (IS_ERR_VALUE(res))
                goto err_pt_alloc;
        mutex_init(&hd_surf->lock);
        hd_surf->width = w;
        hd_surf->height = h;
        hd_surf->sz = w * h;
        hd_surf->interlock = 1;
        hd_surf->dirty = 0;

        return hd_surf;

err_pt_alloc:
        kfree(hd_surf);
err_kzalloc:
        return ERR_PTR(res);
}

void hd_res_free_surface(struct hd_device *hd_dev, struct hd_surface * hd_surf)
{
        spin_lock(&hd_dev->lock);
        list_del(&hd_surf->surf_list);
        spin_unlock(&hd_dev->lock);
        
        hd_free_pagetable(hd_dev, &hd_surf->pagetable);
        kfree(hd_surf);
}

struct hd_texture * hd_res_create_texture(struct hd_device *hd_dev,
                uint64_t data_ptr, uint32_t sz, uint16_t h)
{
        long res;
        struct hd_texture *hd_text;
        int pages_num;

        hd_text = kzalloc(sizeof(struct hd_texture), GFP_KERNEL);
        if (hd_text == NULL) {
                res = -ENOMEM;
                HD_ERR("hd_texture kzalloc failed!\n");
                goto err_kzalloc;
        }
        
        pages_num = (sz + HD_PAGE_SIZE - 1) / HD_PAGE_SIZE;
        res = hd_alloc_pagetable(hd_dev, &hd_text->pagetable, pages_num);
        if (IS_ERR_VALUE(res))
                goto err_pt_alloc;
        hd_text->size = sz;
        hd_text->height = h;

        res = hd_copy_to_pages(hd_text->pagetable.pages, (void *) data_ptr, 
                                sz, 0, HD_PAGE_SIZE);
        if (IS_ERR_VALUE(res) || (res != sz)) {
                HD_ERR("copy_to_pages error: %ld\n", res);
                res = -EFAULT;
                goto err_cpy_to_pages;
        }
        return hd_text;

err_cpy_to_pages:
        hd_free_pagetable(hd_dev, &hd_text->pagetable);
err_pt_alloc:
        kfree(hd_text);
err_kzalloc:
        return ERR_PTR(res);
}

void hd_res_free_texture(struct hd_device *hd_dev, struct hd_texture * hd_text)
{
        hd_free_pagetable(hd_dev, &hd_text->pagetable);
        kfree(hd_text);
}

struct hd_flat * hd_res_create_flat(struct hd_device *hd_dev, uint64_t data_ptr)
{
        long res;
        struct hd_flat *hd_flat;
        hd_flat = kzalloc(sizeof(struct hd_flat), GFP_KERNEL);
        if (hd_flat == NULL) {
                res = -ENOMEM;
                HD_ERR("hd_flat kzalloc failed!\n");
                goto err_kzalloc;
        }

        res = hd_alloc_page(hd_dev, &hd_flat->buffer);
        if (IS_ERR_VALUE(res))
                goto err_pt_alloc;
        res = hd_copy_to_pages(&hd_flat->buffer, (void *) data_ptr, 
                                HD_PAGE_SIZE, 0, HD_PAGE_SIZE);
        if (IS_ERR_VALUE(res) || (res != HD_PAGE_SIZE)) {
                HD_ERR("Create FLAT error: %ld\n", res);
                res = -EFAULT;
                goto err_cpy_to_pages;
        }
        HD_INFO("Flat texture done!\n");
        return hd_flat;

err_cpy_to_pages:
        hd_free_page(hd_dev, &hd_flat->buffer);
err_pt_alloc:
        kfree(hd_flat);
err_kzalloc:
        return ERR_PTR(res);
}

void hd_res_free_flat(struct hd_device *hd_dev, struct hd_flat *hd_flat)
{
        hd_free_page(hd_dev, &hd_flat->buffer);
        kfree(hd_flat);
}

struct hd_cmap * hd_res_create_cmap(struct hd_device *hd_dev,
                uint64_t data_ptr, uint32_t num)
{
        long res;
        int i;
        unsigned long count;
        struct hd_cmap *hd_cmap;

        HD_INFO("Creaing color map...\n");
        hd_cmap = kzalloc(sizeof(struct hd_cmap), GFP_KERNEL);
        if (hd_cmap == NULL) {
                res = -ENOMEM;
                HD_ERR("hd_cmap kzalloc failed!\n");
                goto err_kzalloc;
        }
        hd_cmap->cmaps_num = num;
        for (i = 0; i < num; i++) {
                HD_INFO("Allocating page... %d\n", i);
                res = hd_alloc_cmap(hd_dev, &hd_cmap->cmaps[i]);
                if (IS_ERR_VALUE(res))
                        goto err_pt_alloc;
        }
        count = num * HD_CMAP_SIZE;
        res = hd_copy_to_pages(hd_cmap->cmaps, (void *) data_ptr, 
                                count, 0, HD_CMAP_SIZE);
        if (IS_ERR_VALUE(res) || (res != count)) {
                HD_ERR("copy_to_pages error: %ld\n", res);
                goto err_cpy_to_pages;
        }
        return hd_cmap;

err_cpy_to_pages:
        for (i=i-1; i >= 0; i--)
                hd_free_cmap(hd_dev, &hd_cmap->cmaps[i]);
err_pt_alloc:
        kfree(hd_cmap);
err_kzalloc:
        return ERR_PTR(res);
}

void hd_res_free_cmap(struct hd_device *hd_dev, struct hd_cmap *hd_cmap)
{
        int i;
        for (i = 0; i < hd_cmap->cmaps_num; i++)
                hd_free_cmap(hd_dev, &hd_cmap->cmaps[i]);
        kfree(hd_cmap);
}
