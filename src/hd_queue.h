/*
 * HardDoom™ device command queue interface
 */

#ifndef _HD_QUEUE_H_
#define _HD_QUEUE_H_

#include "structs/hd_queue.h"
#include "structs/hd_pages.h"

void hd_queue_init(struct hd_queue *queue, struct hd_page *page);

int hd_queue_acquire_interruptible(struct hd_queue *queue);
int hd_queue_push_cmd_interruptible(struct hd_queue *queue, uint32_t cmd);
int hd_queue_sync_interruptible(struct hd_queue *queue);

void hd_queue_acquire(struct hd_queue *queue);
void hd_queue_push_cmd(struct hd_queue *queue, uint32_t cmd);
void hd_queue_sync(struct hd_queue *queue);

void hd_queue_leave(struct hd_queue *queue);

void hd_queue_irq_fence(struct hd_queue *queue);
void hd_queue_irq_free_space_event(struct hd_queue *queue);

#endif // _HD_QUEUE_H_
