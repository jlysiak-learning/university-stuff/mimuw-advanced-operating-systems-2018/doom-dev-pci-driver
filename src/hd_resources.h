#ifndef _HD_RES_IFACE_H_
#define _HD_RES_IFACE_H_

#include "structs/hd_device.h"
#include "structs/hd_resources.h"

/*      Extract correct data pointer    */

#define get_surface(res_ptr) \
        ((struct hd_surface *) (res_ptr)->resource)
#define get_texture(res_ptr) \
        ((struct hd_texture *) res_ptr->resource)
#define get_flat(res_ptr) \
        ((struct hd_flat *) res_ptr->resource)
#define get_cmap(res_ptr) \
        ((struct hd_cmap *) res_ptr->resource)

#define res_name(res_ptr) \
        (res_ptr->name)

#define kobj_to_hdres(kobj_ptr) \
        container_of(kobj_ptr, struct hd_resource, kobj);


#define res_set_read_req(res_ptr) \
        (res_ptr->flags |= HD_RES_FLAG_READ)
#define res_get_read_req(res_ptr) \
        (res_ptr->flags & HD_RES_FLAG_READ)
#define res_clr_read_req(res_ptr) \
        (res_ptr->flags &= ~HD_RES_FLAG_READ)

/*      Create and free resources       */

struct hd_resource * hd_res_create_resource(struct hd_device *hd_dev,
                uint8_t type, void * resource);
void hd_res_free_resource(struct hd_resource *hd_res);

struct hd_surface * hd_res_create_surface(struct hd_device *hd_dev,
                uint16_t w, uint16_t h);
void hd_res_free_surface(struct hd_device *hd_dev, struct hd_surface *hd_surf);


struct hd_texture * hd_res_create_texture(struct hd_device *hd_dev,
                uint64_t data_ptr, uint32_t sz, uint16_t h);
void hd_res_free_texture(struct hd_device *hd_dev, struct hd_texture *hd_text);


struct hd_flat * hd_res_create_flat(struct hd_device *hd_dev, uint64_t data_ptr);
void hd_res_free_flat(struct hd_device *hd_dev, struct hd_flat *hd_flat);


struct hd_cmap * hd_res_create_cmap(struct hd_device *hd_dev, 
                uint64_t data_ptr, uint32_t num);
void hd_res_free_cmap(struct hd_device *hd_dev, struct hd_cmap *hd_cmap);

#endif
