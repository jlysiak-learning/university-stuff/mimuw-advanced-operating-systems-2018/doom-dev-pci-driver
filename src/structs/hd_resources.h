#ifndef _HD_RES_H_
#define _HD_RES_H_

#include <linux/types.h>
#include <linux/mutex.h>
#include <linux/kobject.h>
#include "hd_pages.h"
#include "../hd_conf.h"

#define HD_RES_TYPE_SURF        0
#define HD_RES_TYPE_TEXT        1
#define HD_RES_TYPE_FLAT        2
#define HD_RES_TYPE_CMAP        3


/* One struct...
 * ... to hold them all. */
struct hd_resource {
        /* Acces to parent and sysfs.*/
        struct kobject kobj;

        spinlock_t lock;

        /* Device resource index for naming */
        uint16_t idx;

        /* Resource file descriptor. */
        int fd;        

        uint8_t type;

        /* Resource name */
        char name[32];
        
        /* Pointer to specified resources. */
        void *resource;
};

/* HardDoom™ surface object */
struct hd_surface {
        struct hd_pagetable pagetable;

        uint32_t sz;
	uint16_t width;
	uint16_t height;

        /* Used to prevent W when R and 
         * R when W.                    */
        struct mutex lock;

        /* Accessed holding surface mutex.
         * If surface is dirty, FENCE sync
         * is required before read. */
        uint8_t dirty;
        
        /* Accessed holding queue mutex.
         * Indicates that interlock was sent. 
         * Each use as DST clears it. 
         * On COPY_RECT, if INTERLOCK is sent
         * this flag is cleared in all surfaces. */
        uint8_t interlock;

        /* All surfaces are connected in list.
         * If INTERLOCK is sent, interate over
         * and set interlock flag. */
        struct list_head surf_list;
};

/*      Read only structures. No locking required.      */

/* HardDoom™ texture object */
struct hd_texture {
        struct hd_pagetable pagetable;	
        uint32_t size;
	uint16_t height;
};

/* HardDoom™ flat texture object */
struct hd_flat {
        /* Flat texture has size excatly of one page */
        struct hd_page buffer;
};

/* HardDoom™ color maps object */
struct hd_cmap {
        /* Number of cmaps */
        uint16_t cmaps_num;
        /* Small pages of color maps */
        struct hd_page cmaps[HD_CMAPS_MAX];
};

#endif // _HD_RES_H_
