/*
 * HardDoom™ device context
 */
#ifndef _HD_CTX_H_
#define _HD_CTX_H_

#include <linux/types.h>

struct hd_ctx {

        /* DMA addresses of pagetables */
        dma_addr_t surf_dst;
        dma_addr_t surf_src;
        dma_addr_t texture;

        /* Active surface dimensions 
         * SURF_DIMS */
        uint32_t width;
        uint32_t height;
        
        /* Texture dimensions */
        uint32_t text_sz;
        uint32_t text_h;

        /* Used flat texture. */
        dma_addr_t flat_addr;
        
        dma_addr_t cmap_main;
        int cmap_main_idx;

        dma_addr_t cmap_tran;
        int cmap_tran_idx;

        /* Params */
        uint8_t  color;
        uint8_t flags;

        uint16_t x_a;
        uint16_t y_a;

        uint16_t x_b;
        uint16_t y_b;

        uint32_t ustart;
        uint32_t vstart;

        uint32_t vstep;
        uint32_t ustep;
                
};

#endif
