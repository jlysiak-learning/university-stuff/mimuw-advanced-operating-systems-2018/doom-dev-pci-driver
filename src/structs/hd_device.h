#ifndef _HD_DEVICE_H_
#define _HD_DEVICE_H_

#include <linux/kernel.h>
#include <linux/kobject.h>
#include <linux/pci.h>
#include <linux/semaphore.h>
#include <linux/dma-mapping.h>
#include <linux/spinlock.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/irq.h>
#include <linux/completion.h>
#include <linux/list.h>

#include "hd_queue.h"

/* Harddoom device main struct
 * Created in hd_probe. */
struct hd_device {
        /* Reference counting and sysfs purpose */
        struct kobject kobj;
        /* Semaphore for waiting on users to exit before remove.
         * down-ed in hd_remove.*/
        struct semaphore sem_remove;
        spinlock_t lock;
       
        /* Char dev driver interface. */
        struct cdev cdev;
        
        /* PCI device */
        struct pci_dev *pci_dev;
        /* BAR register. */
        void __iomem *bar;

        /* DMA pools.
         * full - HD_PAGE_SIZE allocations
         * cmap - CMAP_SIZE allocations */
        struct dma_pool *full_dma_pool;
        struct dma_pool *cmap_dma_pool;

        struct hd_queue queue;
        uint32_t surf_num;
        uint32_t text_num;
        uint32_t flat_num;
        uint32_t cmap_num;
       
        /* List of all surfaces */
        struct list_head surf_list_head;
};

#define cdev_to_hddev(cdev_ptr) \
        container_of(cdev_ptr, struct hd_device, cdev)
#define kobj_to_hddev(kobj_ptr) \
        container_of(kobj_ptr, struct hd_device, kobj)
#define queue_to_hddev(q_ptr) \
        container_of(q_ptr, struct hd_device, queue)
#define hd_name(hd_ptr) \
        kobject_name(&hd_ptr->kobj)
#endif // _HD_DEVICE_H_
