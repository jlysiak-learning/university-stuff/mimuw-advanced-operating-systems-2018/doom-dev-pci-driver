#ifndef _HD_PAGES_H_
#define _HD_PAGES_H_

#include <linux/types.h>
#include "../hd_conf.h"


struct hd_page {
        void *cpu_addr;
        dma_addr_t dma_addr;
};

typedef uint32_t hd_pte_t;

struct hd_pagetable {
        /* Page table entries DMA address */
        dma_addr_t dma_ents;

        /* Page table entries CPU pointer 
         * Holds DMA-availabe memory of maximal size. */
        hd_pte_t *cpu_ents;

        /* Handles to allocated pages. 
         * Information about number of allocated pages
         * is encoded in entries as VALID flag set to 1 */
        struct hd_page pages[HD_PAGES_MAX];
};

#endif // _HD_PAGES_H_
