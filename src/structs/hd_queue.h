#ifndef _HD_QUEUE_STRUCT_H_
#define _HD_QUEUE_STRUCT_H_

#include <linux/types.h>
#include <linux/spinlock.h>
#include <linux/wait.h>
#include <linux/mutex.h>
#include <linux/completion.h>

#include "hd_pages.h"
#include "hd_ctx.h"

/* HardDoom™ command queue */
struct hd_queue {
        spinlock_t internal_lock;
        struct mutex lock;
        struct hd_page buff;
        
        uint16_t b;
        uint16_t l;

        uint32_t async_counter;
        
        wait_queue_head_t space_waiters;
        struct hd_ctx ctx;

        /* FENCE SYNCING */
        uint32_t fence_sent;
        uint32_t fence_pending;
        uint32_t fence_last;
        wait_queue_head_t fence_waiters;
};

#endif // _HD_QUEUE_STRUCT_H_
