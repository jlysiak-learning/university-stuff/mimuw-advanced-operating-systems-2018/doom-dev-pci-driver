#ifndef _HD_UTILS_H_
#define _HD_UTILS_H_

#include <linux/dma-mapping.h>
#include "structs/hd_device.h"
#include "structs/hd_pages.h"

/* Pages operations */

int hd_alloc_page(struct hd_device *hd_dev, struct hd_page *page);

void * hd_alloc_page2(struct hd_device *hd_dev, dma_addr_t *dma_addr);

int hd_alloc_cmap(struct hd_device *hd_dev, struct hd_page *page);

void hd_free_page(struct hd_device *hd_dev, struct hd_page *page);

void hd_free_page2(struct hd_device *hd_dev, void * cpu_addr, 
                dma_addr_t dma_addr);

void hd_free_cmap(struct hd_device *hd_dev, struct hd_page *page);

/* Page tables operations */

int hd_alloc_pagetable(struct hd_device *hd_dev, 
                struct hd_pagetable *pt, int pages_num);

void hd_free_pagetable(struct hd_device *hd_dev, 
                struct hd_pagetable *pt);

long hd_copy_to_pages(struct hd_page *pages, void __user *from, 
                unsigned long n, unsigned long offset,  unsigned long page_sz);

long hd_copy_from_pages(struct hd_page *pages, void __user *to,
                unsigned long n, unsigned long offset, unsigned long page_sz);
#endif
