/*
 * HardDoom™ device context management.
 * 
 * Device registers are updated only if
 * saved and new values are different.
 */

#ifndef _HD_CTX_IFACE_H_
#define _HD_CTX_IFACE_H_

#include "structs/hd_queue.h"
#include "structs/hd_pages.h"

void hd_ctx_reset(struct hd_queue *queue);
void hd_ctx_restore(struct hd_queue *queue);

void hd_ctx_set_dest_surf_pt(struct hd_queue *queue, struct hd_pagetable *pt);
void hd_ctx_set_src_surf_pt(struct hd_queue *queue, struct hd_pagetable *pt);
void hd_ctx_set_text_pt(struct hd_queue *queue, struct hd_pagetable *pt);
void hd_ctx_set_flat(struct hd_queue *queue, dma_addr_t addr);

void hd_ctx_set_surf_dims(struct hd_queue *queue, int w, int h);
void hd_ctx_set_text_dims(struct hd_queue *queue, uint32_t sz, uint16_t h);

void hd_ctx_set_xy_a(struct hd_queue *queue, uint16_t x, uint16_t y);
void hd_ctx_set_xy_b(struct hd_queue *queue, uint16_t x, uint16_t y);
void hd_ctx_set_color(struct hd_queue *queue, uint8_t color);

void hd_ctx_set_ustep(struct hd_queue *queue, uint32_t val);
void hd_ctx_set_ustart(struct hd_queue *queue, uint32_t val);

void hd_ctx_set_vstep(struct hd_queue *queue, uint32_t val);
void hd_ctx_set_vstart(struct hd_queue *queue, uint32_t val);

void hd_ctx_set_cmap(struct hd_queue *queue, struct hd_page *cmaps, uint8_t idx);
void hd_ctx_set_trans_cmap(struct hd_queue *queue, struct hd_page *cmaps, uint8_t idx);

void hd_ctx_set_draw_params(struct hd_queue *queue, uint8_t flags);

#endif // _HD_CTX_IFACE_H_
