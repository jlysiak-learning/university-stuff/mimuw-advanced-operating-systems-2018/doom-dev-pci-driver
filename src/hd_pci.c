/*
 * HARDDOOM driver PCI subsystem operations.
 */

#include <linux/pci.h>
#include <linux/interrupt.h>
#include <linux/kernel.h>

#include "structs/hd_device.h"
#include "ext/harddoom.h"
#include "hd_conf.h"
#include "hd_common.h"
#include "hd_ll.h"
#include "hd_pci.h"
#include "hd_cdev.h"
#include "hd_queue.h"
#include "hd_ctx.h"


/* Dynamic alloc of minor numbers. 
 * Protected by `hd_idr_lock` */
static DEFINE_IDR(hd_idr);
static DEFINE_SPINLOCK(hd_idr_lock);
static const struct pci_device_id hd_id_table[] = {
        {PCI_DEVICE(HARDDOOM_VENDOR_ID, HARDDOOM_DEVICE_ID)}, 
        {0}
};

/* Releasing device's kobject along
 * with making sure that all users exited. */
static void hd_dev_kobj_release(struct kobject *kobj);
static struct kobj_type hd_dev_kobj_type = {
        .release = hd_dev_kobj_release
};

struct pci_driver hd_pci_driver = {
        .name = HD_PCI_DRV_NAME,
        .id_table = hd_id_table,
        .probe = hd_probe,
        .remove = hd_remove,
        .suspend = hd_suspend,
        .resume = hd_resume
};

dev_t hd_dev_major;
struct class hd_dev_class = {
        .name = HD_DEV_NAME,
        .owner = THIS_MODULE
};

/* Here we wait for closing all resources */
static void hd_dev_kobj_release(struct kobject *kobj)
{
        struct hd_device *hd_dev;
        hd_dev = kobj_to_hddev(kobj);
        kobject_put(kobj->parent);
        HD_INFO("Last handle to %s freed!\n", hd_name(hd_dev));
        up(&hd_dev->sem_remove);
}

int hd_probe(struct pci_dev *pci_dev, const struct pci_device_id *id)
{
        struct hd_device *hd_dev;
        struct device *device;
        long res;
        int minor;

        HD_INFO("Probing PCI device...");

        /* Alloc zeroed memory chunk... I do not
         * set some values to zero again. */
        hd_dev = kzalloc(sizeof(struct hd_device), GFP_KERNEL);
        if (hd_dev == NULL) {
                HD_ERR("kzmalloc - not enough memory!");
                return -ENOMEM;
        }
 
        hd_dev->pci_dev = pci_dev;
        cdev_init(&hd_dev->cdev, &hd_cdev_fops);
        
        res = pci_enable_device(pci_dev);
        if (IS_ERR_VALUE(res)) {
                HD_ERR("pci_enable_device error %lu\n", res);
                goto err_pci_en_drv;
        }
        res = pci_request_regions(pci_dev, HD_DEV_NAME);
        if (IS_ERR_VALUE(res)) {
                HD_ERR("pci_request_regions error %lu\n", res);
                goto err_pci_rq_reg;
        } 
        hd_dev->bar = pci_iomap(pci_dev, 0, 0);
        if (hd_dev->bar == NULL) {
                HD_ERR("pci_iomap failed!\n");
                res = -ENOMEM;
                goto err_pci_iomap;
        }
        
        /* Initialize struct
         * and reset device */
        hd_ll_reset(hd_dev);
        
        // Enable DMA
        pci_set_master(pci_dev);
        res = pci_set_dma_mask(pci_dev, HD_DMA_BMASK);
        if (IS_ERR_VALUE(res)) {
                HD_ERR("pci_set_dma_mask error %lu\n", res);
                goto err_pci_set_dma_mask;
        }
        res = pci_set_consistent_dma_mask(pci_dev, HD_DMA_BMASK);
        if (IS_ERR_VALUE(res)) {
                HD_ERR("pci_set_consistent_dma_mask error %lu\n", res);
                goto err_pci_set_dma_mask;
        }

       /* Create two DMA pools.
        * One for HD_PAGE_SIZE allocations which
        * will be the most commonly used and 
        * second for small allocations for color maps */
        hd_dev->full_dma_pool = dma_pool_create(HD_DEV_NAME "_FULL",
                        &pci_dev->dev, HD_PAGE_SIZE, HD_PAGE_SIZE, 0);
        BUG_ON(hd_dev->full_dma_pool == NULL);
        if (!hd_dev->full_dma_pool){
                HD_ERR("full dma_pool_create error\n");
                res = -ENOMEM;
                goto err_full_dma_pool_create;
        }
        hd_dev->cmap_dma_pool = dma_pool_create(HD_DEV_NAME "_CMAP",
                        &pci_dev->dev, HD_CMAP_SIZE, HD_CMAP_SIZE, 0);
        BUG_ON(hd_dev->cmap_dma_pool == NULL);
        if (!hd_dev->cmap_dma_pool){
                HD_ERR("cmap dma_pool_create error\n");
                res = -ENOMEM;
                goto err_cmap_dma_pool_create;
        }
        /* DMA is enabled and pools created.
         * Finalize device setting up. */
        res = hd_ll_init(hd_dev);
        if (IS_ERR_VALUE(res)) {
                HD_ERR("device initialization failed: %ld\n", res);
                goto err_ll_init;
        }

        res = request_irq(pci_dev->irq, hd_irq, IRQF_SHARED,
                        HD_PCI_DRV_NAME, hd_dev);
        if (IS_ERR_VALUE(res)) {
                HD_ERR("request_irq error %ld\n", res);
                goto err_req_irq;
        }

        spin_lock(&hd_idr_lock);
        minor = idr_alloc(&hd_idr, hd_dev, 0, HD_DEV_MAX_CNT, GFP_KERNEL);
        spin_unlock(&hd_idr_lock);
        if (IS_ERR_VALUE((long) minor)) {
                res = minor;
                HD_ERR("idr alloc error %ld\n", res);
                goto err_idr_alloc;
        }

        res = kobject_init_and_add(&hd_dev->kobj, &hd_dev_kobj_type, 
                        &pci_dev->dev.kobj, "%s%d", HD_DEV_NAME, minor);
        if (IS_ERR_VALUE(res)) {
                HD_ERR("kobject_init_and_add error %ld\n", res);
                goto err_kobj_init_and_add;
        }
        res = cdev_add(&hd_dev->cdev, hd_dev_major + minor, 1);
        if (IS_ERR_VALUE(res)) {
                HD_ERR("cdev_add error %ld\n", res);
                goto err_cdev_add;
        }
        
        /* Add cdev (our pci dev interface) as a child to make sure
         * that nobody uses our device before removing it by PCI subsystem. */
        res = kobject_add(&hd_dev->cdev.kobj, &hd_dev->kobj, 
                        "%s%d", HD_CDEV_NAME, minor);
        if (IS_ERR_VALUE(res)) {
                HD_ERR("kobject_add error %ld\n", res);
                goto err_kobj_add;
        }
        device = device_create(&hd_dev_class, &pci_dev->dev, hd_dev->cdev.dev,
                        hd_dev, "%s%d", HD_DEV_NAME, minor);
        if (IS_ERR(device)) {
                res = PTR_ERR(device);
                HD_ERR("device_create error %ld\n", res);
                goto err_dev_create;
        }

        pci_set_drvdata(pci_dev, hd_dev);

        /* Enable and start */
        hd_ll_start(hd_dev);
        
        /* Reset device context when started. */
        hd_queue_acquire(&hd_dev->queue);
        hd_ctx_reset(&hd_dev->queue);
        hd_queue_leave(&hd_dev->queue);

        HD_INFO("PCI device `%s%d` started successfuly!", HD_DEV_NAME, minor);
        return 0;

err_dev_create:
        kobject_del(&hd_dev->cdev.kobj);
err_kobj_add:
        cdev_del(&hd_dev->cdev);
err_cdev_add:
        kobject_del(&hd_dev->kobj);
err_kobj_init_and_add:
        spin_lock(&hd_idr_lock);
        idr_remove(&hd_idr, minor);
        spin_unlock(&hd_idr_lock);
err_idr_alloc:
        free_irq(pci_dev->irq, hd_dev);
err_req_irq:
        dma_pool_free(hd_dev->full_dma_pool, 
                        hd_dev->queue.buff.cpu_addr,
                        hd_dev->queue.buff.dma_addr);
err_ll_init:
        dma_pool_destroy(hd_dev->cmap_dma_pool);
err_cmap_dma_pool_create:
        dma_pool_destroy(hd_dev->full_dma_pool);
err_full_dma_pool_create:
err_pci_set_dma_mask:
        pci_iounmap(pci_dev, hd_dev->bar);
err_pci_iomap:
        pci_release_regions(pci_dev);
err_pci_rq_reg:
        pci_disable_device(pci_dev);
err_pci_en_drv:
        kfree(hd_dev);

        return res;
}

void hd_remove(struct pci_dev *pci_dev)
{
        struct hd_device *hd_dev;
        int minor;

        hd_dev = pci_get_drvdata(pci_dev);
        BUG_ON(hd_dev == NULL);
        
        minor = MINOR(hd_dev->cdev.dev);
        HD_INFO("Removing PCI device `%s%d`...\n", HD_DEV_NAME, minor);

        /* Don't allow to open device anymore 
         * `/dev/doomX` would disappear... */
        device_destroy(&hd_dev_class, hd_dev->cdev.dev);
        cdev_del(&hd_dev->cdev);
        /* Unlink from hierarchy */
        kobject_del(&hd_dev->kobj);
        
        kobject_put(&hd_dev->kobj);
        /* Wait for users to exit */
        down(&hd_dev->sem_remove);
        kobject_del(&hd_dev->cdev.kobj);

        /* Stop PCI device */
        hd_ll_stop(hd_dev);
        
        spin_lock(&hd_idr_lock);
        idr_remove(&hd_idr, minor);
        spin_unlock(&hd_idr_lock);
        free_irq(pci_dev->irq, hd_dev);
        dma_pool_free(hd_dev->full_dma_pool, 
                        hd_dev->queue.buff.cpu_addr,
                        hd_dev->queue.buff.dma_addr);
        dma_pool_destroy(hd_dev->cmap_dma_pool);
        dma_pool_destroy(hd_dev->full_dma_pool);
        pci_iounmap(pci_dev, hd_dev->bar);
        pci_release_regions(pci_dev);
        pci_disable_device(pci_dev);
        kfree(hd_dev);
        HD_INFO("PCI device `%s%d` freed!", HD_DEV_NAME, minor);
}


int hd_suspend (struct pci_dev *dev, pm_message_t state)
{
        struct hd_device *hd_dev;
        int minor;

        hd_dev = pci_get_drvdata(dev);
        BUG_ON(hd_dev == NULL);

        minor = MINOR(hd_dev->cdev.dev);
        HD_INFO("Suspending  PCI device `%s%d`... Message: %d\n", 
                        HD_DEV_NAME, minor, state.event);
        
        /* Suspending device.
         * Wait uninterruptible until all commands are done.
         * Because we are in suspend no new process will try to 
         * access queue and they were woked up by fake signal (if any has slept) */
        hd_queue_sync(&hd_dev->queue);
        
        /* Ok, device is synced. Active context is saved in structures. */
        hd_ll_stop(hd_dev);

        return 0;
}


int hd_resume (struct pci_dev *dev) 
{
        struct hd_device *hd_dev;
        int minor;

        hd_dev = pci_get_drvdata(dev);
        BUG_ON(hd_dev == NULL);

        minor = MINOR(hd_dev->cdev.dev);
        HD_INFO("Resuming PCI device `%s%d`...\n", HD_DEV_NAME, minor);
        /* Upload code */
        hd_ll_reset(hd_dev);
        /* Restore device */
        hd_ll_restore(hd_dev);
        /* Enable and start */
        hd_ll_start(hd_dev);
        /* Restore device context */
        hd_ctx_reset(&hd_dev->queue);
        return 0;
}
