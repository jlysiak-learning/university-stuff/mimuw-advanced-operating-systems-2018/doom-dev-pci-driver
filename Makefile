KDIR ?= /home/jacek/Workspace/zso-2017/hshare/lib/modules/4.9.13/build

default:
	$(MAKE) -C $(KDIR) M=$$PWD/src
	cp src/harddoom.ko .

install:
	$(MAKE) -C $(KDIR) M=$$PWD/src modules_install

clean:
	$(MAKE) -C $(KDIR) M=$$PWD/src clean
	rm harddoom.ko

todo:
	@find . -type f -name *.[ch] -exec grep --color -n -2 TODO {} +
